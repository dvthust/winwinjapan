<?php

namespace App\Helpers;

class ApiUtil {

    const STATUS_SUCCESS = 1;
    const STATUS_FAILURE = 0;

    const ERROR_101 = 101; //Bad request
    const ERROR_102 = 102; //Error param
    const ERROR_103 = 103; //Authen failure

    const ERROR_200 = 200; //OK - Response to a successful GET, PUT, PATCH or DELETE.
    const ERROR_201 = 201; //Created - Response to a POST that results in a creation.
    const ERROR_204 = 204; //No Content - Response to a successful request that won't be returning a body (like a DELETE request)

}