<?php

namespace App\Helpers;

class AppUtil {

    const FIRST_USER_ID = 1;
    const DOMAIN_APP = '/';
    const DOMAIN_ADMIN = '/admin';
    const DEFAULT_DATE_FORMAT = "d-m-Y";

    /** ------------------PERMISSION----------------- */
    const PERMISSION_MANAGE_USERS = "manage_users";

    /** --------------------ROLE--------------------- */
    const SUPER_ADMIN_ROLE_ID = 1;
    const SUPER_ADMIN_ROLE_NAME = 'Super Admin';
    const SUPER_ADMIN_ROLE_SLUG = 'super-admin';

    const ADMINISTRATOR_ROLE_ID = 2;
    const ADMINISTRATOR_ROLE_NAME = 'Administrator';
    const ADMINISTRATOR_ROLE_SLUG = 'admin';

    const EDITOR_ROLE_ID = 3;
    const EDITOR_ROLE_NAME = 'Editor';
    const EDITOR_ROLE_SLUG = 'editor';

    const AUTHOR_ROLE_ID = 4;
    const AUTHOR_ROLE_NAME = 'Author';
    const AUTHOR_ROLE_SLUG = 'author';

    const CONTRIBUTOR_ROLE_ID = 5;
    const CONTRIBUTOR_ROLE_NAME = 'Contributor';
    const CONTRIBUTOR_ROLE_SLUG = 'contributor';

    const SUBSCRIBER_ROLE_ID = 6;
    const SUBSCRIBER_ROLE_NAME = 'Subscriber';
    const SUBSCRIBER_ROLE_SLUG = 'subscriber';

    const KEYWORD_APPEND_SLUG_IF_EXISTS = '-2';

    static function getLabelRole($role)
    {
        $html = '';
        switch ($role) {
            case self::SUPER_ADMIN_ROLE_ID:
                $html = '<span class="label label-danger">Super Admin</span>';
                break;
            case self::ADMINISTRATOR_ROLE_ID:
                $html = '<span class="label label-danger">Administrator</span>';
                break;
            case self::EDITOR_ROLE_ID:
                $html = '<span class="label label-warning">Editor</span>';
                break;
            case self::AUTHOR_ROLE_ID:
                $html = '<span class="label label-primary">Author</span>';
                break;
            case self::CONTRIBUTOR_ROLE_ID:
                $html = '<span class="label label-success">Contributor</span>';
                break;
            case self::SUBSCRIBER_ROLE_NAME:
                $html = '<span class="label label-success">Subscriber</span>';
                break;
            default:
                break;
        }
        return $html;
    }

    // ---------- COMMON --------------------------------------//

    const COMMON_PAGINATION = 10;

    static function getNumberFormat($number, $decimal = 0)
    {
        return number_format($number, $decimal);
    }

    static function getMoneyFormat($money, $decimal = 0)
    {
        return number_format($money, $decimal, "," ,".") . "₫";
    }

    // --------- STATUS - FLAG ------------------------------------//
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    static function getLabelStatus($status)
    {
        switch ($status) {
            case self::STATUS_ACTIVE:
                $html = '<span class="label label-success">Active</span>';
                break;
            case self::STATUS_DEACTIVE:
                $html = '<span class="label label-danger">Blocked</span>';
                break;
            default:
                $html = '';
                break;
        }
        return $html;
    }

    const FLAG_SHOW = 1;
    const FLAG_HIDE = 0;

    // --------- MESSAGE ------------------------------------//
    const MSG_SUCCESS = "success";
    const MSG_WARNING = "warning";
    const MSG_ERROR = "danger";

    static function getHtmlMsgText($type, $addText = '')
    {
        $html = '<div class="alert alert-%s alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-%s"></i> %s %s</h4>
                </div>';
        switch ($type) {
            case self::MSG_SUCCESS:
                $html = sprintf($html, self::MSG_SUCCESS, "check", "Thành công !", $addText);
                break;
            case self::MSG_WARNING:
                $html = sprintf($html, self::MSG_WARNING, "warning", "Không tìm thấy mục này !", $addText);
                break;
            case self::MSG_ERROR:
                $html = sprintf($html, self::MSG_ERROR, "ban", "Có lỗi xảy ra !", $addText);
                break;
            default:
                $html = '';
                break;
        }
        return $html;
    }

    // ------------- Convert DATE ----------------------------- //
    const DATE_DD_MM_YYYY_SLASH = "d/m/Y";
    const DATE_DD_MM_YYYY_H_I_SLASH = "d/m/Y H:i";
    const DATE_YYYY_MM_DD_MINUS = "Y-m-d";
    const DATE_YYYY_MM_DD_H_I_MINUS = "Y-m-d H:i";

    static function convertDateDB($date, $returnFormat = self::DATE_DD_MM_YYYY_SLASH)
    {
        return date($returnFormat, strtotime($date));
    }

    static function convertDate($date, $format = self::DATE_DD_MM_YYYY_SLASH, $returnFormat = self::DATE_DD_MM_YYYY_SLASH)
    {
        $dateArr = [];
        if ($format == self::DATE_YYYY_MM_DD_MINUS) {
            $dateArr = explode('-', $date);
            list($year, $month, $day) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'YYYY/MM/DD') {
            $dateArr = explode('/', $date);
            list($year, $month, $day) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'YYYY.MM.DD') {
            $dateArr = explode('.', $date);
            list($year, $month, $day) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'DD-MM-YYYY') {
            $dateArr = explode('-', $date);
            list($day, $month, $year) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == self::DATE_DD_MM_YYYY_SLASH) {
            $dateArr = explode('/', $date);
            list($day, $month, $year) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'DD.MM.YYYY') {
            $dateArr = explode('.', $date);
            list($day, $month, $year) = isset($dateArr[2]) ? $dateArr : null;
        }

        if ($format == 'MM-DD-YYYY') {
            $dateArr = explode('-', $date);
            list($month, $day, $year) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'MM/DD/YYYY') {
            $dateArr = explode('/', $date);
            list($month, $day, $year) = isset($dateArr[2]) ? $dateArr : null;
        }
        if ($format == 'MM.DD.YYYY') {
            $dateArr = explode('.', $date);
            list($month, $day, $year) = isset($dateArr[2]) ? $dateArr : null;
        }

        return date($returnFormat, strtotime($year . "-" . $month . "-" . $day));
    }

    static function generateRandomString($num = 32, $extraText = NULL)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $num) . $extraText;
    }

    static function getSqlSelectDistance($curLat, $curLng, $latField = 'latitude', $lngField = 'longitude')
    {
        return "( 6371 * acos( cos( radians(" . doubleval($curLat) . ") ) * cos( radians( " . $latField . " ) ) * cos( radians( " . $lngField . " ) - radians(" . doubleval($curLng) . ") ) + sin( radians(" . doubleval($curLat) . ") ) * sin( radians( " . $latField . " ) ) ) ) AS distance";
    }

    /*****************************************************************************
     ******************************--Make-Slug--**********************************
     ****************************************************************************/
    /**
     * Default map of accented and special characters to ASCII characters
     *
     * @var array
     */
    protected static $_transliteration = array(
        '/À|Á|Â|Ã|Å|Ǻ|Ā|Ă|Ą|Ǎ/' => 'A',
        '/Æ|Ǽ/' => 'AE',
        '/Ä/' => 'Ae',
        '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
        '/Ð|Ď|Đ/' => 'D',
        '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/' => 'E',
        '/Ĝ|Ğ|Ġ|Ģ|Ґ/' => 'G',
        '/Ĥ|Ħ/' => 'H',
        '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ|І/' => 'I',
        '/Ĳ/' => 'IJ',
        '/Ĵ/' => 'J',
        '/Ķ/' => 'K',
        '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
        '/Ñ|Ń|Ņ|Ň/' => 'N',
        '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ/' => 'O',
        '/Œ/' => 'OE',
        '/Ö/' => 'Oe',
        '/Ŕ|Ŗ|Ř/' => 'R',
        '/Ś|Ŝ|Ş|Ș|Š/' => 'S',
        '/ẞ/' => 'SS',
        '/Ţ|Ț|Ť|Ŧ/' => 'T',
        '/Þ/' => 'TH',
        '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
        '/Ü/' => 'Ue',
        '/Ŵ/' => 'W',
        '/Ý|Ÿ|Ŷ/' => 'Y',
        '/Є/' => 'Ye',
        '/Ї/' => 'Yi',
        '/Ź|Ż|Ž/' => 'Z',
        '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª/' => 'a',
        '/ä|æ|ǽ/' => 'ae',
        '/ç|ć|ĉ|ċ|č/' => 'c',
        '/ð|ď|đ/' => 'd',
        '/è|é|ê|ë|ē|ĕ|ė|ę|ě/' => 'e',
        '/ƒ/' => 'f',
        '/ĝ|ğ|ġ|ģ|ґ/' => 'g',
        '/ĥ|ħ/' => 'h',
        '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı|і/' => 'i',
        '/ĳ/' => 'ij',
        '/ĵ/' => 'j',
        '/ķ/' => 'k',
        '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
        '/ñ|ń|ņ|ň|ŉ/' => 'n',
        '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º/' => 'o',
        '/ö|œ/' => 'oe',
        '/ŕ|ŗ|ř/' => 'r',
        '/ś|ŝ|ş|ș|š|ſ/' => 's',
        '/ß/' => 'ss',
        '/ţ|ț|ť|ŧ/' => 't',
        '/þ/' => 'th',
        '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ/' => 'u',
        '/ü/' => 'ue',
        '/ŵ/' => 'w',
        '/ý|ÿ|ŷ/' => 'y',
        '/є/' => 'ye',
        '/ї/' => 'yi',
        '/ź|ż|ž/' => 'z',

        //Vietnamese
        '/á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ/' => 'a',
        '/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ/' => 'e',
        '/í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị/' => 'i',
        '/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ/' => 'o',
        '/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự/' => 'u',
        '/ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ/' => 'y',
    );

    /**
     * Returns a string with all spaces converted to underscores (by default), accented
     * characters converted to non-accented characters, and non word characters removed.
     *
     * @param string $string the string you want to slug
     * @param string $replacement will replace keys in map
     * @return string
     * @link http://book.cakephp.org/2.0/en/core-utility-libraries/inflector.html#Inflector::slug
     */
    public static function slug($string, $replacement = '-')
    {

        $quotedReplacement = preg_quote($replacement, '/');

        $merge = array(
            '/[^\s\p{Zs}\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => '',
            '/[\s\p{Zs}]+/mu' => $replacement,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );

        $map = static::$_transliteration + $merge;
        $string = preg_replace(array_keys($map), array_values($map), $string);
        return strtolower($string);
    }
}