<?php

namespace App\Http\ViewComposers;

use App\Eloquent\Slideshow;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BannerComposer
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param \Illuminate\Contracts\View\View $view
     */
    public function compose(View $view)
    {
        $banners = Slideshow::all();
        $view->with('banners', $banners);
    }
}