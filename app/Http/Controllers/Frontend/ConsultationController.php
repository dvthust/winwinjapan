<?php

namespace App\Http\Controllers\Frontend;

class ConsultationController extends DefaultController
{
    public function index()
    {
        return view('frontend.pages.consultation');
    }
}
