<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

class ErrorHandlerController extends DefaultController
{
    public function errorCode404()
    {
        return view('frontend.errors.404');
    }

    public function errorCode405()
    {
        return view('frontend.errors.405');
    }
}