<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TranslationController extends DefaultController
{
    /**
     * Change session locale
     * @param  Request $request
     * @return Response
     */
    public function changeLocale(Request $request)
    {
        $this->validate($request, ['lang' => 'required|in:vi,ja']);

        session(['lang' => $request->lang]);

        return redirect()->back();
    }
}
