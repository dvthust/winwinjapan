<?php

namespace App\Http\Controllers\Frontend;


use App\Eloquent\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends DefaultController
{
    public function index(){

        return view('frontend.pages.contact');
    }

    /**
     * Create post info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('frontend/common.validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        Contact::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect()->back();
    }
}
