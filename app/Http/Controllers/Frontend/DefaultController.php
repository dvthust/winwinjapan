<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class DefaultController extends Controller
{
    /**
     * Show the home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.pages.home');
    }

    /**
     * Show the korean_study_abroad page.
     *
     * @return \Illuminate\Http\Response
     */
    public function korean_study_abroad()
    {
        return view('frontend.pages.korean_study_abroad');
    }

    /**
     * Show the japan_study_abroad page.
     *
     * @return \Illuminate\Http\Response
     */
    public function japan_study_abroad()
    {
        return view('frontend.pages.japan_study_abroad');
    }

    /**
     * Show the apprentice page.
     *
     * @return \Illuminate\Http\Response
     */
    public function apprentice()
    {
        return view('frontend.pages.apprentice');
    }

    /**
     * Show the engineer page.
     *
     * @return \Illuminate\Http\Response
     */
    public function engineer()
    {
        return view('frontend.pages.engineer');
    }

    /**
     * Show the engineer page.
     *
     * @return \Illuminate\Http\Response
     */
    public function engineer_mechanical()
    {
        return view('frontend.pages.engineer_mechanical');
    }

}
