<?php

namespace App\Http\Controllers\Frontend;


use App\Eloquent\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends DefaultController
{
    public function view($slug, $id)
    {
        $order = Order::where('id', $id)->where('slug' ,$slug)->first();

        if (!$order) {
            return abort(404);
        }

        return view('frontend.pages.order_detail',[
            'order' => $order,
            'slug' => $slug,
            'id' => $id
        ]);
    }
}
