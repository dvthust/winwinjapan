<?php

namespace App\Http\Controllers\Frontend;

use App\Eloquent\Like;
use App\Eloquent\Post;
use App\Eloquent\Video;
use Illuminate\Support\Facades\Auth;

class LikeController extends DefaultController
{
    public function likeVideo($id)
    {
        // here you can check if video exists or is valid or whatever

        $this->handleLike(Video::class, $id);
        return redirect()->back();
    }

    public function likePost($id)
    {
        // here you can check if post exists or is valid or whatever

        $this->handleLike(Post::class, $id);
        return redirect()->back();
    }

    public function handleLike($type, $id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }
}
