<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

class ErrorHandlerController extends DefaultController
{
    public function errorCode404()
    {
        return view('backend.errors.404');
    }

    public function errorCode405()
    {
        return view('backend.errors.405');
    }
}