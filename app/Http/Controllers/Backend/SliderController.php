<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Slideshow;
use App\Helpers\AppUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    /**
     * Show the slider management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slideshow::paginate(AppUtil::COMMON_PAGINATION);

        return view('backend.pages.settings.slider.index', [
            'sliders' => $sliders
        ]);
    }

    /**
     * Delete specific contact.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $slider = Slideshow::find($id);

        if ($slider) {
            $slider->delete();
            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Delete Multi Contact
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post')) {
            foreach ($data_ids as $id) {
                $item = Slideshow::find($id);
                $item->delete();
            }

            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Show the edit page.
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slideshow::find($id);

        if($slider){
            return view('backend.pages.settings.slider.edit', [
                'slider' => $slider
            ]);
        }else{
            flash("Dữ liệu không tồn tại !", 'error');
            return view('backend.errors.404');
        }
    }

    /**
     * Update info
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $slider = Slideshow::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('common.validation.required', ["atribute" => "tiêu đề"])
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $slider->fill($data)->save();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect()->back();
    }

    /**
     * Show the create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.settings.slider.create');
    }

    /**
     * Create info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('common.validation.required', ["atribute" => "tiêu đề"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        Slideshow::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.settings.slider.index'));
    }
}
