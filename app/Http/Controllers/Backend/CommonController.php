<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\AppUtil;
use Illuminate\Http\Request;

class CommonController extends DefaultController
{

    /**
     * Localization for JS
     * @return mixed
     */
    public function getLangJs(){
        $temp = 'var APP_LANG={};';
        foreach (trans('backend/langjs') as $key => $value) {
            $temp .= 'APP_LANG.' . $key . ' = "' . $value . '";';
        }

        return response($temp)->header('Content-Type', 'text/javascript');
    }

    public function setActive(Request $request)
    {
        $model = $request->model;
        $item_id = $request->item_id;
        $active = $request->active == 'true' ? AppUtil::STATUS_ACTIVE : AppUtil::STATUS_DEACTIVE;

        $data = [
            'status' => AppUtil::STATUS_DEACTIVE,
            'msg' => trans('backend/common.data.update.failed')
        ];

        if ($model && $item_id && $request->isMethod('post')) {
            $model = "App\Eloquent\\" . $model;
            $item = $model::find($item_id);
            if ($item) {
                $item->status = $active;
                $item->save();

                $data['status'] = AppUtil::STATUS_ACTIVE;
                $data['msg'] = trans('backend/common.data.update.success');
            }
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function setHighlight(Request $request)
    {
        $model = $request->model;
        $item_id = $request->item_id;
        $active = $request->active == 'true' ? AppUtil::STATUS_ACTIVE : AppUtil::STATUS_DEACTIVE;

        $data = [
            'status' => AppUtil::STATUS_DEACTIVE,
            'msg' => trans('backend/common.data.update.failed')
        ];

        if ($model && $item_id && $request->isMethod('post')) {
            $model = "App\Eloquent\\" . $model;
            $item = $model::find($item_id);
            if ($item) {
                $item->highlight = $active;
                $item->save();

                $data['status'] = AppUtil::STATUS_ACTIVE;
                $data['msg'] = trans('backend/common.data.update.success');
            }
        }

        return response()->json([
            'data' => $data
        ]);
    }

}
