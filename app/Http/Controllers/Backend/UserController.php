<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\User;
use App\Helpers\AppUtil;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends DefaultController
{
    /**
     * Display a listing of User.
     *User
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }

        $users = User::all();

        return view('backend.pages.users.index', compact('users'));
    }

    /**
     * User profile page
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function profile($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }

        $roles = Role::where("id", "!=", AppUtil::SUPER_ADMIN_ROLE_ID)->get();
        $user = User::findOrFail($id);
        $user_role_ids = $user->roles->pluck("id")->toArray();

        return view('backend.pages.users.profile', compact('user', 'roles', 'user_role_ids'));
    }

    public function updateProfile(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('backend/validation.user.name.required')
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $data["birthday"] = AppUtil::convertDate($request->birthday, 'd/m/Y', 'Y/m/d');
        $user->fill($data)->save();

        //Update Role
//        $roles = $request->input('roles') ? $request->input('roles') : [];
//        $user->syncRoles($roles);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect()->back();
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $roles = Role::where("id", "!=", AppUtil::SUPER_ADMIN_ROLE_ID)->get();

        return view('backend.pages.users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ], [
            'name.required' => trans('backend/validation.user.name.required'),
            'email.required' => trans('backend/validation.user.email.required'),
            'email.email' => trans('backend/validation.user.email.email'),
            'email.unique' => trans('backend/validation.user.email.unique'),
            'password.required' => trans('backend/validation.user.password.required'),
            'password.min' => trans('backend/validation.user.password.min'),
            'password.confirmed' => trans('backend/validation.user.password.confirmed')
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $data["birthday"] = AppUtil::convertDate($request->birthday, 'd/m/Y', 'Y/m/d');
        $data["password"] = $request->password;
        $user = User::create($data);

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.users.index'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $user = User::findOrFail($id);
        $user->delete();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect()->back();
    }

    /**
     * Delete Multi User
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post')) {
            User::destroy($data_ids);

            flash(trans('backend/common.data.update.success'), 'success');
        }

        return redirect()->back();
    }

    /**
     * Update User Password
     */
    public function updatePassword(Request $request){
        $data = $request->all();

        $user = User::find($request->user_id);

        $validator = Validator::make($data, [
            'password' => 'required|min:6|confirmed'
        ], [
            'password.required' => trans('backend/validation.user.password.required'),
            'password.min' => trans('backend/validation.user.password.min'),
            'password.confirmed' => trans('backend/validation.user.password.confirmed')
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $data["password"] = $request->password;
        $user->fill($data)->save();
        flash(trans('backend/common.data.update.success'), 'success');

        return redirect()->back();
    }
}
