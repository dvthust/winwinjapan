<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class DefaultController extends Controller
{
    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.index');
    }
}
