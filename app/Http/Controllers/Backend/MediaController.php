<?php

namespace App\Http\Controllers\Backend;

class MediaController extends DefaultController
{
    /**
     * Show the media management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.media.index');
    }
}
