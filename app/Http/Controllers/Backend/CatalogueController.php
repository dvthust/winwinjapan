<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Catalogue;
use App\Helpers\AppUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatalogueController extends Controller
{
    /**
     * Show the post category management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Catalogue::paginate(AppUtil::COMMON_PAGINATION);

        return view('backend.pages.recruits.catalogues.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Delete specific post category.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $cat = Catalogue::find($id);

        if ($cat) {
            $cat->delete();
            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Delete Multi Post Catgory
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post')) {
            foreach ($data_ids as $id) {
                $item = Catalogue::find($id);
                $item->delete();
            }

            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Show the post category edit page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Catalogue::find($id);

        if ($cat) {
            return view('backend.pages.recruits.catalogues.edit', [
                'cat' => $cat
            ]);
        } else {
            flash("Dữ liệu không tồn tại !", 'error');
            return view('backend.errors.404');
        }
    }

    /**
     * Update post category info
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $cat = Catalogue::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $cat->fill($data)->save();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.catalogues.index'));
    }

    /**
     * Show the post category create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.recruits.catalogues.create');
    }

    /**
     * Create post category info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        Catalogue::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.catalogues.index'));
    }
}
