<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Order;
use App\Eloquent\Catalogue;
use App\Helpers\AppUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Show the post management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Catalogue::all();
        $posts = Order::where("status", AppUtil::STATUS_ACTIVE);

        if($request->get("catalogue_id")){
            $posts = $posts->where("catalogue_id", $request->catalogue_id);
        }

        if($request->get("time")){
            if($request->time == Order::$FILTER_TIME_TYPE_TO_DAY){
                $posts = $posts->whereDate('created_at', date("Y-m-d"));
            }else if($request->time == Order::$FILTER_TIME_TYPE_THIS_WEEK){
                $posts = $posts->whereDate('created_at', ">=", date("Y-m-d", strtotime('monday this week')));
                $posts = $posts->whereDate('created_at', "<=", date("Y-m-d"));
            }else if($request->time == Order::$FILTER_TIME_TYPE_THIS_MONTH){
                $posts = $posts->whereMonth("created_at", date("m"));
                $posts = $posts->whereYear("created_at", date("Y"));
            }
        }

        if($request->get("title")){
            $posts->where("name_vi", "like", "%" . $request->title . "%");
            $posts->orWhere("name_ja", "like", "%" . $request->title . "%");
        }

        $posts = $posts->paginate(AppUtil::COMMON_PAGINATION);
        return view('backend.pages.recruits.index', [
            'posts' => $posts,
            'categories' => $categories
        ]);
    }

    /**
     * Delete specific post.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $post = Order::find($id);

        if ($post) {
            $post->delete();
            flash(trans('backend/common.data.update.success'), 'success');
        }

        return redirect()->back();
    }

    /**
     * Delete Multi Order
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post')) {
            foreach ($data_ids as $id) {
                $item = Order::find($id);
                $item->delete();
            }

            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Show the post edit page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $categories = Catalogue::all();

        if($order){
            return view('backend.pages.recruits.edit', [
                'order' => $order,
                'categories' => $categories
            ]);
        }else{
            flash("Dữ liệu không tồn tại !", 'error');
            return view('backend.errors.404');
        }
    }

    /**
     * Update post info
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $post = Order::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $post->fill($data)->save();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.orders.index'));
    }

    /**
     * Show the post create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Catalogue::all();

        return view('backend.pages.recruits.create',[
            'categories' => $categories
        ]);
    }

    /**
     * Create post info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $data["created_by"] = Auth::user()->id;

        Order::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.orders.index'));
    }
}
