<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\AppUtil;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;

class RolesController extends DefaultController
{
    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }

        $roles = Role::all();

        return view('backend.pages.users.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $permissions = Permission::get()->pluck('name', 'name');

        return view('backend.pages.users.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param StoreRolesRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function store(StoreRolesRequest $request)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $role = Role::create($request->except('permission'));
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->givePermissionTo($permissions);

        return redirect()->route('admin.roles.index');
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $permissions = Permission::get()->pluck('name', 'name');

        $role = Role::findOrFail($id);

        return view('backend.pages.users.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update Role in storage.
     *
     * @param UpdateRolesRequest $request
     * @param                    $id
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update(UpdateRolesRequest $request, $id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->update($request->except('permission'));
        $permissions = $request->input('permission') ? $request->input('permission') : [];
        $role->syncPermissions($permissions);

        return redirect()->route('admin.roles.index');
    }


    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('admin.roles.index');
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
