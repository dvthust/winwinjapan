<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\AppUtil;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\StorePermissionsRequest;
use App\Http\Requests\Admin\UpdatePermissionsRequest;

class PermissionsController extends DefaultController
{
    /**
     * Display a listing of Permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }

        $permissions = Permission::all();

        return view('backend.pages.users.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating new Permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        return view('backend.pages.users.permissions.create');
    }

    /**
     * Store a newly created Permission in storage.
     *
     * @param StorePermissionsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function store(StorePermissionsRequest $request)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        Permission::create($request->all());

        return redirect()->route('admin.permissions.index');
    }


    /**
     * Show the form for editing Permission.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $permission = Permission::findOrFail($id);

        return view('backend.pages.users.permissions.edit', compact('permission'));
    }

    /**
     * Update Permission in storage.
     *
     * @param UpdatePermissionsRequest $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update(UpdatePermissionsRequest $request, $id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        return redirect()->route('admin.permissions.index');
    }


    /**
     * Remove Permission from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        $permission = Permission::findOrFail($id);
        $permission->delete();

        return redirect()->route('admin.permissions.index');
    }

    /**
     * Delete all selected Permission at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows(AppUtil::PERMISSION_MANAGE_USERS)) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Permission::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
