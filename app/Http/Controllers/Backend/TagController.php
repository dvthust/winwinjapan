<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Tag;
use App\Helpers\AppUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    /**
     * Show the tag management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Tag::paginate(AppUtil::COMMON_PAGINATION);

        return view('backend.pages.tags.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the tag create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.tags.create');
    }

    /**
     * Create post category info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('common.validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        Tag::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.tags.index'));
    }

    /**
     * Show the tag edit page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Tag::find($id);

        if ($item) {
            return view('backend.pages.tags.edit', [
                'item' => $item
            ]);
        } else {
            flash("Dữ liệu không tồn tại !", 'error');
            return view('backend.errors.404');
        }
    }

    /**
     * Update Tag info
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $cat = Tag::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required'
        ], [
            'name.required' => trans('common.validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $cat->fill($data)->save();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.tags.index'));
    }

    /**
     * Delete specific Tag.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $cat = Tag::find($id);

        if ($cat) {
            $cat->delete();
            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Delete Multi Post Catgory
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post'))
        {
            Tag::destroy($data_ids);

            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }
}
