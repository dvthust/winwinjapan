<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Post;
use App\Eloquent\Category;
use App\Helpers\AppUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Show the post management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();
        $posts = Post::where("status", AppUtil::STATUS_ACTIVE);

        if($request->get("category_id")){
            $posts = $posts->where("category_id", $request->category_id);
        }

        if($request->get("time")){
            if($request->time == Post::$FILTER_TIME_TYPE_TO_DAY){
                $posts = $posts->whereDate('created_at', date("Y-m-d"));
            }else if($request->time == Post::$FILTER_TIME_TYPE_THIS_WEEK){
                $posts = $posts->whereDate('created_at', ">=", date("Y-m-d", strtotime('monday this week')));
                $posts = $posts->whereDate('created_at', "<=", date("Y-m-d"));
            }else if($request->time == Post::$FILTER_TIME_TYPE_THIS_MONTH){
                $posts = $posts->whereMonth("created_at", date("m"));
                $posts = $posts->whereYear("created_at", date("Y"));
            }
        }

        if($request->get("name_vi")){
            $posts->where("name_vi", "like", "%" . $request->name_vi . "%");
            $posts->orWhere("name_ja", "like", "%" . $request->name_vi . "%");
        }

        $posts = $posts->paginate(AppUtil::COMMON_PAGINATION);
        return view('backend.pages.posts.index', [
            'posts' => $posts,
            'categories' => $categories
        ]);
    }

    /**
     * Delete specific post.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if ($post) {
            $post->delete();
            flash(trans('backend/common.data.update.success'), 'success');
        }

        return redirect()->back();
    }

    /**
     * Delete Multi Post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMulti(Request $request)
    {
        $data_ids = explode(',', $request->data_ids);

        if (count($data_ids) && $request->isMethod('post')) {
            foreach ($data_ids as $id) {
                $item = Post::find($id);
                $item->delete();
            }

            flash(trans('backend/common.data.update.success'), 'success');
            return redirect()->back();
        }
    }

    /**
     * Show the post edit page.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::all();

        if($post){
            return view('backend.pages.posts.edit', [
                'post' => $post,
                'categories' => $categories
            ]);
        }else{
            flash("Dữ liệu không tồn tại !", 'error');
            return view('backend.errors.404');
        }
    }

    /**
     * Update post info
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if($validator->fails()){
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $post->fill($data)->save();

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.posts.index'));
    }

    /**
     * Show the post create page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('backend.pages.posts.create',[
            'categories' => $categories
        ]);
    }

    /**
     * Create post info
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name_vi' => 'required'
        ], [
            'name_vi.required' => trans('validation.required', ["atribute" => "tên"])
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first(), 'error');
            return redirect()->back();
        }

        $data["created_by"] = Auth::user()->id;

        Post::create($data);

        flash(trans('backend/common.data.update.success'), 'success');
        return redirect(route('admin.posts.index'));
    }
}
