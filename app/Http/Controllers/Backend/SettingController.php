<?php

namespace App\Http\Controllers\Backend;

use App\Eloquent\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Show setting page
     */
    function general(){

        $setting = Setting::first();

        return view('backend.pages.settings.general.index',[
            'setting' => $setting
        ]);
    }


    /**
     * Update general setting info
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function updateGeneral(Request $request){

        $setting = Setting::first();
        $data = $request->all();

        if($setting){
            $setting->fill($data)->save();
        }else {
            $setting = Setting::create($data);
        }

        flash(trans('backend/common.data.update.success'), 'success');
        return view('backend.pages.settings.general.index',[
            'setting' => $setting
        ]);
    }
}
