<?php

namespace App\Http\Middleware;

use App\Helpers\AppUtil;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->hasRole(AppUtil::SUPER_ADMIN_ROLE_NAME) || Auth::user()->hasRole(AppUtil::ADMINISTRATOR_ROLE_NAME)))
        {
            return $next($request);
        }

        return redirect(route('index'));
    }
}
