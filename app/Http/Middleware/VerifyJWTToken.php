<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token = JWTAuth::getToken();
            $user = JWTAuth::authenticate($token);
            if (!$user) {
                return response()->json([
                    'msg' => 'user_not_found'
                ], 404);
            }
        }catch (JWTException $e) {
            if($e instanceof TokenExpiredException) {
                $newToken = JWTAuth::refresh($token);

                return response()->json([
                    'msg' => 'token_expired',
                    'token' => $newToken
                ], $e->getStatusCode());
            }else if ($e instanceof TokenInvalidException) {
                return response()->json([
                    'msg' => 'token_invalid'
                ], $e->getStatusCode());
            }else{
                return response()->json(['error'=>'Token is required']);
            }
        }

        return $next($request);
    }
}
