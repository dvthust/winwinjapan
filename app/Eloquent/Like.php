<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'likeable_id',
        'likeable_type',
        'type',
    ];

    /**
     * Get all of the posts that are assigned this like.
     */
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'likeable');
    }

    /**
     * Get all of the videos that are assigned this like.
     */
    public function videos()
    {
        return $this->morphedByMany(Video::class, 'likeable');
    }

}
