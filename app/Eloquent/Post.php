<?php

namespace App\Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use Sluggable;

    public static $FILTER_TIME_TYPE_TO_DAY = 1;
    public static $FILTER_TIME_TYPE_THIS_WEEK = 2;
    public static $FILTER_TIME_TYPE_THIS_MONTH = 3;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table used by the model
     * @var string
     */
    protected $table = "posts";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'category_id',
        'name_vi',
        'name_ja',
        'slug',
        'short_des_vi',
        'short_des_ja',
        'content_vi',
        'content_ja',
        'feature_image',
        'thumb_image',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'updated_by',
        'created_by',
        'highlight',
        'view',
        'status'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name_vi'
            ]
        ];
    }

    /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     *  grab all users that have liked a post.
     * @return mixed
     */
    public function likes()
    {
        return $this->morphToMany(User::class, 'likeable')->whereDeletedAt(null);
    }

    /**
     *  Check if the current user has liked the post or not.
     * @return bool
     */
    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }
}