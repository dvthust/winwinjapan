<?php

namespace App\Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    /**
     * The table used by the model
     * @var string
     */
    protected $table = "categories";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id',
        'name_vi',
        'name_ja',
        'slug',
        'description_vi',
        'description_ja',
        'status',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name_vi'
            ]
        ];
    }
}
