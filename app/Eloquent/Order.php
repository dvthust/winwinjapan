<?php

namespace App\Eloquent;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Sluggable;

    public static $FILTER_TIME_TYPE_TO_DAY = 1;
    public static $FILTER_TIME_TYPE_THIS_WEEK = 2;
    public static $FILTER_TIME_TYPE_THIS_MONTH = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'catalogue_id',
        'code',
        'exam_date',
        'exp_date',
        'name_vi',
        'name_ja',
        'slug',
        'short_des_vi',
        'short_des_ja',
        'content_vi',
        'content_ja',
        'feature_image',
        'thumb_image',
        'image',
        'images',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'information',
        'title_seo',
        'highlight',
        'position',
        'updated_by',
        'created_by',
        'total',
        'video',
        'status'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name_vi'
            ]
        ];
    }

    /**
     * Automatically creates fields
     *
     * @param  string  $value
     * @return void
     */
    public function setExamDateAttribute($value)
    {
        $this->attributes['exam_date'] = $value ? Carbon::createFromFormat('d/m/Y', $value) : null;
    }

    /**
     * Automatically creates fields
     *
     * @param  string  $value
     * @return void
     */
    public function setExpDateAttribute($value)
    {
        $this->attributes['exp_date'] = $value ? Carbon::createFromFormat('d/m/Y', $value) : null;
    }
}
