<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'email',
        'address',
        'telephone',
        'position',
        'status'
    ];
}
