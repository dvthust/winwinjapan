<?php

namespace App\Eloquent;

use App\Helpers\AppUtil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'last_login',
        'last_visit'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'nickname',
        'gender',
        'birthday',
        'phone',
        'address',
        'about',
        'avatar',
        'token',
        'last_login',
        'last_login_ip',
        'last_visit',
        'settings',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Custom format for the last login date
     *
     * @param $value
     * @return string
     */
    public function getLastLoginAttribute($value)
    {
        return Carbon::parse($value)->format(AppUtil::DEFAULT_DATE_FORMAT);
    }

    /**
     * Make sure that we get an array from JSON string
     *
     * @param $value
     * @return array
     */
    public function getSettingsAttribute($value) {
        return json_decode($value, true);
    }

    /**
     * Encode an array to a JSON string
     *
     * @param $value
     */
    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = json_encode($value);
    }

    /**
     * Get all the posts that a specific user has liked
     * @return mixed
     */
    public function likedPosts()
    {
        return $this->morphedByMany(Post::class, 'likeable')->whereDeletedAt(null);
    }
}
