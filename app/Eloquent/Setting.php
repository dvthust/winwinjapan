<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The table used by the model
     * @var string
     */
    protected $table = "settings";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'slogan',
        'address',
        'address2',
        'email',
        'phone',
        'facebook',
        'twitter',
        'skype',
        'zalo',
        'hotline',
        'google_map',
        'format_date',
        'format_time',
        'meta_key',
        'meta_des',
        'contactinfo'
    ];
}
