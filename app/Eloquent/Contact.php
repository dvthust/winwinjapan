<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    /**
     * The table used by the model
     * @var string
     */
    protected $table = "contacts";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'content'
    ];


}
