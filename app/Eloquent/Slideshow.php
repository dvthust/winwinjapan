<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'link',
        'position',
        'status'
    ];
}
