<?php

namespace App\Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;
    use Sluggable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table used by the model
     * @var string
     */
    protected $table = "videos";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'category_id',
        'title',
        'slug',
        'url',
        'short_des',
        'content',
        'feature_image',
        'thumb_image',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'updated_by',
        'created_by',
        'highlight',
        'status'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get all of the video's comments.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Get all of the tags for the video.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
}
