<?php

namespace App\Eloquent;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Catalogue extends Model
{
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name_vi',
        'name_ja',
        'slug',
        'description_vi',
        'description_ja',
        'level',
        'feature_image',
        'thumb_image',
        'image',
        'home',
        'menu_top',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'position',
        'highlight',
        'lft',
        'rght',
        'status'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name_vi'
            ]
        ];
    }
}
