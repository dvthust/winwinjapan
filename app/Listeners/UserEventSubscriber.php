<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Http\Request;

class UserEventSubscriber
{
    /**
     * Create the event listener.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle user login events.
     * @param $event
     */
    public function onUserLogin(Login $event)
    {
        $user = $event->user;
        $user->last_login = Carbon::now();
        $user->last_login_ip = $this->request->ip();
        $user->save();
    }

    /**
     * Handle user logout events.
     * @param $event
     */
    public function onUserLogout(Logout $event)
    {
        $event->user->last_visit = Carbon::now();
        $event->user->save();
    }

    /**
     * Register the listeners for the subscriber.
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}