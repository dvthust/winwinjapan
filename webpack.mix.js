let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    //Backend
    .sass('resources/assets/sass/backend/admin.scss', 'public/css')
    .scripts([
        'resources/assets/js/backend/common.js',
        'resources/assets/js/backend/admin.js'
    ], 'public/js/bundle.js')
    .scripts([
        'resources/assets/js/backend/ck.js'
    ], 'public/js/ck.js')

    //Frontend
    .scripts([
        'resources/assets/js/frontend/common.js',
        'resources/assets/js/frontend/app.js'
    ], 'public/js/all.js')
    .sass('resources/assets/sass/frontend/app.scss', 'public/css');
