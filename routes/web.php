<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Route::group([
    'namespace' => 'Frontend'
], function () {
    Route::post('changelocale', ['as' => 'changelocale', 'uses' => 'TranslationController@changeLocale']);
    Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
    Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

    Route::get('/', ['as' => 'index', 'uses' => 'DefaultController@index']);

    Route::get('/gioi-thieu', ['as' => 'introduce', 'uses' => 'DefaultController@index']);
    Route::get('/gioi-thieu-cong-ty', ['as' => 'introduce.company', 'uses' => 'DefaultController@index']);
    Route::get('/cac-hoat-dong-ngoai-khoa', ['as' => 'activities', 'uses' => 'DefaultController@index']);

    Route::get('/gioi-thieu', ['as' => 'introduce', 'uses' => 'DefaultController@index']);
    Route::get('/lien-he', ['as' => 'contact', 'uses' => 'ContactController@index']);
    Route::post('/lien-he', ['as' => 'contact.post', 'uses' => 'ContactController@store']);

    Route::get('/goc-tu-van', ['as' => 'consultation.index', 'uses' => 'ConsultationController@index']);
    Route::get('/du-hoc-han-quoc', ['as' => 'korean.study.abroad.index', 'uses' => 'DefaultController@korean_study_abroad']);
    Route::get('/du-hoc-nhat-ban', ['as' => 'korean.study.abroad.index', 'uses' => 'DefaultController@japan_study_abroad']);
    Route::get('/thuc-tap-sinh', ['as' => 'apprentice.index', 'uses' => 'DefaultController@apprentice']);
    Route::get('/ky-su', ['as' => 'engineer.index', 'uses' => 'DefaultController@engineer']);
    Route::get('/ky-su-co-khi', ['as' => 'engineer.mechanical.index', 'uses' => 'DefaultController@engineer_mechanical']);
    Route::get('/ky-su/{slug}.{id}', ['as' => 'engineer.mechanical.view', 'uses' => 'OrderController@view']);

    Route::get('/post', ['as' => 'post', 'uses' => 'PostController@index']);
});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix'    => 'admin',
    'namespace' => 'Backend',
    'middleware' => ['auth', 'admin'],
    'as'        => 'admin.'
], function () {
    Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
    Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

    Route::get('/', ['as' => 'home.index', 'uses' => 'DefaultController@index']);
    Route::resource('permissions', 'PermissionsController');
    Route::resource('roles', 'RolesController');
    Route::get('/media', ['as' => 'media.index', 'uses' => 'MediaController@index']);

    Route::group([
        'as' => 'common.',
        'prefix' => 'common'
    ], function(){
        Route::get('/getLangJs', ['as' => 'getLangJs', 'uses' => 'CommonController@getLangJs']);
        Route::post('/setActive', ['as' => 'setActive', 'uses' => 'CommonController@setActive']);
        Route::post('/setHighlight', ['as' => 'setHighlight', 'uses' => 'CommonController@setHighlight']);
    });

    Route::group([
        'prefix'    => 'users',
        'as'        => 'users.',
        '_active_menu' => 'users'
    ], function(){
        Route::get('/', ['as' => 'index', 'uses' => 'UserController@index', '_active_menu' => 'index']);
        Route::get('/create', ['as' => 'create', 'uses' => 'UserController@create']);
        Route::post('/store', ['as' => 'store', 'uses' => 'UserController@store']);
        Route::get('/{id}', ['as' => 'profile', 'uses' => 'UserController@profile']);
        Route::patch('/{id}/update', ['as' => 'profile.update', 'uses' => 'UserController@updateProfile']);
        Route::delete('/{id}/destroy', ['as' => 'destroy', 'uses' => 'UserController@destroy']);
        Route::post('/password/update', ['as' => 'password.update', 'uses' => 'UserController@updatePassword']);
    });

    //Post Category
    Route::resource('categories', 'CategoryController');
    Route::post('/categories/delete/multi', ['as' => 'categories.delete.multi', 'uses' => 'CategoryController@deleteMulti']);

    //Catalogue Order
    Route::resource('catalogues', 'CatalogueController');
    Route::post('/catalogues/delete/multi', ['as' => 'catalogues.delete.multi', 'uses' => 'CatalogueController@deleteMulti']);

    //Post
    Route::post('/posts/delete/multi', ['as' => 'posts.delete.multi', 'uses' => 'PostController@deleteMulti']);
    Route::resource('posts', 'PostController');

    //Order
    Route::post('/orders/delete/multi', ['as' => 'orders.delete.multi', 'uses' => 'OrderController@deleteMulti']);
    Route::resource('orders', 'OrderController');

    //Tag
    Route::resource('tags', 'TagController');
    Route::post('/tags/delete/multi', ['as' => 'tags.delete.multi', 'uses' => 'TagController@deleteMulti']);

    //Support
    Route::resource('supports', 'SupportController');
    Route::post('/supports/delete/multi', ['as' => 'supports.delete.multi', 'uses' => 'SupportController@deleteMulti']);

    //Setting
    Route::group([
        'prefix'    => 'settings',
        'as'        => 'settings.'
    ], function(){
        Route::get('/general', ['as' => 'general.index', 'uses' => 'SettingController@general']);
        Route::post('/general', ['as' => 'general.update', 'uses' => 'SettingController@updateGeneral']);
        Route::get('/slider', ['as' => 'slider.index', 'uses' => 'SliderController@index']);
        Route::get('/slider/create', ['as' => 'slider.create', 'uses' => 'SliderController@create']);
        Route::post('/slider/create', ['as' => 'slider.store', 'uses' => 'SliderController@store']);
        Route::get('/slider/{id}/edit', ['as' => 'slider.edit', 'uses' => 'SliderController@edit']);
        Route::put('/slider/{id}', ['as' => 'slider.update', 'uses' => 'SliderController@update']);
        Route::delete('/slider/{id}', ['as' => 'slider.delete', 'uses' => 'SliderController@delete']);
        Route::post('/slider/delete/multi', ['as' => 'slider.delete.multi', 'uses' => 'SliderController@deleteMulti']);
    });
});
