
//CkFinder
function selectFileWithCKFinder( elementId ) {
    CKFinder.modal( {
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function( finder ) {
            finder.on( 'files:choose', function( evt ) {
                var file = evt.data.files.first();
                var output = document.getElementById( elementId );
                output.value = file.getUrl();
            } );

            finder.on( 'file:choose:resizedImage', function( evt ) {
                var output = document.getElementById( elementId );
                output.value = evt.data.resizedUrl;
            } );
        }
    } );
}

function showMultiImage(obj) {
    var inputId = $(obj).data("target");
    var modal_preview = $("#myModalImage");
    if ($("#" + inputId).val() != "") {
        modal_preview.find("img").attr("src", $("#" + inputId).val());
        modal_preview.modal("show");
    }
}

//Remove Single image;
function delSingleImg(obj){
    $(obj).parent().parent().find("input").val('');
}
