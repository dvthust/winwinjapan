"use strict";

var APP = APP || {};
APP.FE = {};
APP.FE.COMMON = {};

//Application in debug or production mode
APP.FE.COMMON.DEBUG = true;
APP.FE.DISABLE_LOG = function () {
    if(!APP.FE.COMMON.DEBUG){
        var noop = function () {};
        console.log = console.warn = console.error = console.info = noop;
    }
};
APP.FE.DISABLE_LOG();

//

//Handle show contribute panel on footer
$(window).scroll(function() {
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();

    if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
        $('#app__contribute').addClass('active slideInUp').removeClass('slideOutDown');
    }else {
        $('#app__contribute').removeClass("slideInUp").addClass('slideOutDown');
    }
});

$(function () {
    // Handle Scroll Top
    $("div.button-scrolltop").on("click", function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    //Change language
    $(".change-lang-frm .language-item").on("click", function () {
        $(".change-lang-frm").submit();
    });

});