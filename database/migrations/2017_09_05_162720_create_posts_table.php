<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->collation = 'utf8_general_ci';
            $table->charset = 'utf8';

            $table->increments('id');
            $table->integer('category_id')->nullable();
            $table->string('name_vi',  255 )->nullable();
            $table->string('name_ja',  255 )->nullable();
            $table->string('slug',  255 )->unique()->nullable();
            $table->text('short_des_vi')->nullable();
            $table->text('short_des_ja')->nullable();
            $table->mediumText('content_vi')->nullable();
            $table->mediumText('content_ja')->nullable();
            $table->string('feature_image',  255 )->nullable();
            $table->string('thumb_image',  255 )->nullable();
            $table->string('meta_title' )->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('view')->nullable();
            $table->tinyInteger('highlight')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
