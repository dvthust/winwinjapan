<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catalogue_id')->nullable();
            $table->string('code')->nullable();
            $table->dateTime('exam_date')->nullable();
            $table->dateTime('exp_date')->nullable();
            $table->string('name_vi',  255 )->nullable();
            $table->string('name_ja',  255 )->nullable();
            $table->string('slug',  255 )->unique()->nullable();
            $table->text('short_des_vi')->nullable();
            $table->text('short_des_ja')->nullable();
            $table->mediumText('content_vi')->nullable();
            $table->mediumText('content_ja')->nullable();
            $table->string('feature_image',  255 )->nullable();
            $table->string('thumb_image',  255 )->nullable();
            $table->string('image',  255 )->nullable();
            $table->string('images',  255 )->nullable();
            $table->string('meta_title' )->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('information')->nullable();
            $table->text('title_seo')->nullable();
            $table->integer('position')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('total')->nullable();
            $table->string('video')->nullable();
            $table->tinyInteger('highlight')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
