<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->collation = 'utf8_general_ci';
            $table->charset = 'utf8';

            $table->increments('id');
            $table->string('name',  255 )->nullable();
            $table->string('title',  255 )->nullable();
            $table->string('slogan',  255 )->nullable();
            $table->string('phone',  45 )->nullable();
            $table->string('address',  255 )->nullable();
            $table->string('address2',  255 )->nullable();
            $table->string('email',  255 )->nullable();
            $table->string('hotline',  45 )->nullable();
            $table->text('google_map')->nullable();
            $table->string('facebook',  255 )->nullable();
            $table->string('twitter',  255 )->nullable();
            $table->string('zalo',  255 )->nullable();
            $table->string('skype',  255 )->nullable();
            $table->string('format_date',  10 )->default('Y-m-d');
            $table->string('format_time',  10 )->default('H:i');
            $table->text('meta_key')->nullable();
            $table->text('meta_des')->nullable();
            $table->text('contactinfo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
