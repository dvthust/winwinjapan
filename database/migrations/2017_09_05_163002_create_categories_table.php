<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->collation = 'utf8_general_ci';
            $table->charset = 'utf8';

            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('name_vi',  255)->nullable();
            $table->string('name_ja',  255)->nullable();
            $table->string('slug',  255)->unique()->nullable();
            $table->text('description_vi')->nullable();
            $table->text('description_ja')->nullable();
            $table->integer('level')->nullable();
            $table->string('feature_image',  255 )->nullable();
            $table->string('thumb_image',  255 )->nullable();
            $table->boolean('menu_top')->nullable()->default('0');
            $table->string('meta_title' )->nullable();
            $table->string('meta_keyword' )->nullable();
            $table->string('meta_description')->nullable();
            $table->tinyInteger('highlight')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
