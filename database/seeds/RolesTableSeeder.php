<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Helpers\AppUtil;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
            [
                'id' => AppUtil::SUPER_ADMIN_ROLE_ID,
                'name' => AppUtil::SUPER_ADMIN_ROLE_NAME
            ],
            [
                'id' => AppUtil::ADMINISTRATOR_ROLE_ID,
                'name' => AppUtil::ADMINISTRATOR_ROLE_NAME
            ],
            [
                'id' => AppUtil::EDITOR_ROLE_ID,
                'name' => AppUtil::EDITOR_ROLE_NAME
            ],
            [
                'id' => AppUtil::AUTHOR_ROLE_ID,
                'name' => AppUtil::AUTHOR_ROLE_NAME
            ],
            [
                'id' => AppUtil::CONTRIBUTOR_ROLE_ID,
                'name' => AppUtil::CONTRIBUTOR_ROLE_NAME
            ],
            [
                'id' => AppUtil::SUBSCRIBER_ROLE_ID,
                'name' => AppUtil::SUBSCRIBER_ROLE_NAME
            ]
        ];

        foreach ($roles as $role) {
            $role_created = Role::create([
                'id' => $role['id'],
                'name' => $role['name']
            ]);

            if($role["id"] == AppUtil::SUPER_ADMIN_ROLE_ID){
                $role_created->givePermissionTo(AppUtil::PERMISSION_MANAGE_USERS);
            }
        }
    }
}
