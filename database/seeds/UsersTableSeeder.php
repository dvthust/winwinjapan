<?php

use Illuminate\Database\Seeder;
use App\Eloquent\User;
use App\Helpers\AppUtil;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 50)->create()->each(function ($user) {

            $user->token = base64_encode($user->email);

            if($user->id == AppUtil::FIRST_USER_ID){
                $user->name = "Đặng Văn Tuấn";
                $user->email = "dvt.hust@gmail.com";
                $user->password = "123456";

                $user->assignRole(AppUtil::SUPER_ADMIN_ROLE_NAME);
            }else {
                $user->assignRole(AppUtil::SUBSCRIBER_ROLE_NAME);
            }

            $user->save();
        });
    }
}
