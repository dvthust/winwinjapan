<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Eloquent\Tag;
use App\Helpers\AppUtil;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();

        factory(Tag::class, 10)->create()->each(function ($tag) {
            $tag->slug = AppUtil::slug($tag->name);
            $tag->save();
        });
    }
}
