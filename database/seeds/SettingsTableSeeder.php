<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\AppUtil;
use App\Eloquent\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();

        $jsonFile = File::get(storage_path() . '/seed/setting.json');

        $data = json_decode($jsonFile);

        Setting::create([
            'company_name' => $data->company_name,
            'address' => $data->address,
            'email' => $data->email,
            'phone' => $data->phone,
            'technical_support_skype' => $data->technical_support_skype,
            'service_support_skype' => $data->service_support_skype,
            'facebook_address' => $data->facebook_address,
            'zalo_address' => $data->zalo_address,
            'twitter_address' => $data->twitter_address,
            'google_plus_address' => $data->google_plus_address,
            'hotline1' => $data->hotline1,
            'hotline2' => $data->hotline2,
            'short_intro' => $data->short_intro,
            'about_us' => $data->about_us,
            'map_id' => $data->map_id
        ]);

    }
}
