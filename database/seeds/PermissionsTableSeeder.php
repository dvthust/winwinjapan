<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Helpers\AppUtil;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => AppUtil::PERMISSION_MANAGE_USERS]);
    }
}
