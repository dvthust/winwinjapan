<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\AppUtil;
use App\Eloquent\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        $jsonFile = File::get(storage_path() . '/seed/post_cat.json');

        $data = json_decode($jsonFile);
        for ($i = 0; $i < count($data); $i++) {
            Category::create([
                'title' => $data[$i]->name,
                'slug' => AppUtil::slug($data[$i]->name)
            ]);
        }
    }
}
