<?php

use Faker\Generator as Faker;
use App\Helpers\AppUtil;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Eloquent\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = '123456',
        'remember_token' => str_random(10),
        'nickname' => $faker->name,
        'gender' => $faker->numberBetween(1,2),
        'birthday' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'about' => $faker->text(),
        'avatar' => $faker->imageUrl(100, 100),
        'status' => AppUtil::STATUS_DEACTIVE
    ];
});
