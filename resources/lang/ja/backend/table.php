<?php

return [

    'common' => [
        'input_key_search' => 'Nhập từ khoá',
        'search' => 'Tìm kiếm',
        'status' => 'Trạng thái',
        'updated_at' => 'Cập nhật',
        'action' => 'Thao tác',
        'sum' => 'Tổng số',
        'add_new' => 'Thêm mới',
    ],

    'user' => [
        'avatar' => 'Avatar',
        'email' => 'Email',
        'phone' => 'Điện thoại',
        'name' => 'Họ và tên',
        'gender' => [
            'title' => 'Giới tính',
            'male' => 'Nam',
            'female' => 'Nữ',
            'other' => 'Khác'
        ],
        'birthday' => 'Ngày sinh',
        'address' => 'Địa chỉ',
        'password' => 'Mật khẩu',
        'password_confirmation' => 'Nhập lại mật khẩu',
        'profile_info' => 'Thông tin cá nhân',
        'account_info' => 'Thông tin tài khoản',
        'about' => 'Giới thiệu',
        'role' => [
            'title' => 'Quyền hạn',
            'super_admin' => "Super Admin",
            'admin' => 'Admin',
            'director' => 'Tổng biên tập',
            'poster' => 'Cộng tác viên',
            'user' => 'Người dùng'
        ],

    ]
];