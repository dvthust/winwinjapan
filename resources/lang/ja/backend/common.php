<?php

return [

    'data' => [
        'update' => [
            'success' => 'Dữ liệu đã được cập nhật.',
            'failed' => 'Dữ liệu chưa được cập nhật. Xin hãy thử lại sau.'
        ]
    ],

    'button' => [
        'add' => 'Thêm mới',
        'save' => 'Lưu',
        'cancle' => 'Hủy'
    ]
];
