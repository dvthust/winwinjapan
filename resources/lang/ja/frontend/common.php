<?php

return [
    'slogan' => 'ƯƠM MẦM ĐAM MÊ - VƯƠN TỚI TƯƠNG LAI',
    'header' => [
        'home' => 'Home',
        'introduce' => 'Introduce',
        'introduce_company' => 'Introduce Company',
        'activities' => 'Activities',
        'engineer' => 'Engineer',
        'engineer_job' => 'Engineer Job',
        'engineer_job_mechanical' => 'Engineer Job Mechanical',
        'engineer_job_it' => 'Engineer Job It',
        'engineer_mechanical' => 'Engineer Mechanical',
        'apprentice' => 'Apprentice',
        'student_korean' => 'Student Korean',
        'student_japan' => 'Student Japan',
        'consultant' => 'Consultant',
        'contact' => 'Contact',
    ]
];