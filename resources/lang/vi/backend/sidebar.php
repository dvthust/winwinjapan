<?php

return [

    'wellcome' => 'Xin chào',
    'menu_section' => [
        'content_management' => [
            'title' => 'Quản lý nội dung',
            'dashboard' => 'Bảng điều khiển',
            'news' => 'Quản lý tin tức',
            'news_category' => 'Danh mục tin tức',
            'posts' => 'Danh sách bài viết',
            'products' => 'Quản lý sản phẩm',
            'product_category' => 'Danh mục hàng hóa',
            'product_list' => 'Danh sách sản phẩm',
            'order_management' => 'Quản lý đơn hàng',
            'review_contribute' => 'Nhận xét & góp ý',
            'review'=> 'Nhận xét',
            'contribute' => 'Góp ý',
            'noti_and_ad' => 'Thông báo & Ads',
            'notification' => 'Thông báo',
            'advertisement' => 'Quảng cáo',
            'media' => 'Media'
        ],
        'account_and_permission' => [
            'title' => 'Tài khoản và quyền hạn',
            'account' => 'Tài khoản',
            'account_list' => 'Danh sách tài khoản',
            'create_update' => 'Thêm mới,Chỉnh sửa',
            'your_profile' => 'Hồ sơ của bạn',
            'recyclebin' => 'Thùng rác'
        ],
        'configuration' => [
            'title' => 'Cấu hình & giao diện',
            'slide_image' => 'Ảnh slide',
            'general_setting' => 'Thiết lập chung'
        ]
    ]

];
