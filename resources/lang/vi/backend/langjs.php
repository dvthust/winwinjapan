<?php

return [
    'record_selected' => 'đối tượng được chọn',
    'delete_confirm_title' => 'Xác nhận xóa',
    'delete_confirm' => 'Bạn có chắc chắn muốn xóa đối tượng này không ?',
    'delete' => 'Xóa',
    'cancle' => 'Hủy',
    'at_least_one' => 'Bạn phải chọn ít nhất một đối tượng.'
];