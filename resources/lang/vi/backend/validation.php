<?php

return [
    'user' => [
        'name' => [
            'required' => 'Tên không được để trống.',
        ],
        'email' => [
            'required' => 'Địa chỉ email không được để trống.',
            'unique' => 'Địa chỉ email đã tồn tại',
            'email' => 'Địa chỉ email không hợp lệ.'
        ],
        'password' => [
            'required' => 'Mật khẩu không được để trống.',
            'min' => 'Mật khẩu phải có ít nhất 6 kí tự.',
            'confirmed' => 'Mật khẩu nhập lại không đúng.'
        ]
    ]
];