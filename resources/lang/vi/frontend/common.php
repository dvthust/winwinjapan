<?php

return [
    'slogan' => 'ƯƠM MẦM ĐAM MÊ - VƯƠN TỚI TƯƠNG LAI',
    'header' => [
        'home' => 'Trang chủ',
        'introduce' => 'Giới thiệu',
        'introduce_company' => 'Giới thiệu công ty',
        'activities' => 'Các hoạt động ngoại khoá',
        'engineer' => 'Kỹ sư',
        'engineer_job' => 'Quy trình tuyển dụng kỹ sư',
        'engineer_job_mechanical' => 'Tuyển dụng kỹ sư cơ khí',
        'engineer_job_it' => 'Tuyển dụng kỹ sư IT',
        'engineer_mechanical' => 'Kỹ sư cơ khí',
        'apprentice' => 'Thực tập sinh',
        'student_korean' => 'Du học hàn quốc',
        'student_japan' => 'Du học nhật bản',
        'consultant' => 'Góc tư vấn',
        'contact' => 'Liên hệ',
    ]
];