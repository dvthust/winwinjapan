<?php

return [
    'contact' => [
        'name' => 'Họ và tên',
        'email' => 'Địa chỉ email',
        'phone' => 'Số điện thoại',
        'content' => 'Nội dung',
        'contact_info' => 'Thông tin liên hệ',
        'company_name' => 'Công Ty TNHH Hợp Tác Phát Triển Nguồn Nhân Lực WINWINJAPAN',
        'address' => 'Địa chỉ',
        'address_detail' => 'Đường liên cơ - Từ Liêm - Hà Nội',
        'email_detail' => 'hai.xkld.vinafor@gmail.com',
        'phone_detail' => '0977258192',
        'map' => 'Bản đồ',
        'msg_success' => 'Thông tin của bạn đã được gửi ! Xin cám ơn !',
        'send_btn' => 'Gửi'
    ],
    'consultation' => [
        'new_order' => 'Đơn hàng mới'
    ]
];