@extends('frontend.layout.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="login--container">
                <div class="span6 col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <h5 class="title-bg" style="margin-top: 0px;">Reset Password</h5>
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="input-prepend {{ $errors->has('email') ? ' has-error' : '' }}">
                            <span class="add-on">
                                <i class="icon-user"></i>
                            </span>
                                    <input class="span6" id="prependedInput" size="16" type="email"
                                           placeholder="Địa chỉ email" name="email" value="{{ old('email') }}">
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                                <div class="form-group" style="margin-top: 1.5em;">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Send Password Reset Link
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
