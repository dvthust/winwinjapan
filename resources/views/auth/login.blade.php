@extends('frontend.layout.default')

@section('content')

    <div class="row login--container">
        <div class="span6">
            <!--User Login-->
            <h5 class="title-bg" style="margin-top: 0px;">User Login</h5>
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="input-prepend {{ $errors->has('email') ? ' has-error' : '' }}">
                <span class="add-on">
                    <i class="icon-user"></i></span>
                    <input class="span6" id="prependedInput" size="16" type="text" placeholder="Địa chỉ email" name="email" value="{{ old('email') }}">
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <div class="input-prepend {{ $errors->has('password') ? ' has-error' : '' }}" style="margin: 15px 0 10px 0;">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input class="span6" id="appendedPrependedInput" size="16" type="password" placeholder="Mật khẩu" name="password">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block pwd-error">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <div class="login__remember-me">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>
                <div class="login__btn--container">
                    <button class="btn btn-small btn-inverse" type="submit">Đăng nhập</button>
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Quên mật khẩu
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection
