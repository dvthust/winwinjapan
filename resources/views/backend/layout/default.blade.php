<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @yield('title_for_layout')

    <link href="{{ asset('vendor/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('vendor/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/jquery-confirm/jquery-confirm.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" rel="stylesheet">

    <!-- PNotify -->
    <link href="{{ asset('vendor/pnotify/dist/pnotify.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">

    <link href="{{ asset('vendor/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/inspinia/css/style.css') }}" rel="stylesheet">

    <!-- Custom -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body class="">

<div id="wrapper">
    @include('backend.elements.sidebar')
    <div id="page-wrapper" class="gray-bg">
        @include('backend.elements.navigation')

        @yield('content')

        @include('backend.elements.footer')
    </div>
</div>

<script>
    var DOMAIN_ADMIN = '{{ AppUtil::DOMAIN_ADMIN }}';
</script>
<script src="{{ route('admin.common.getLangJs') }}" type="text/javascript"></script>
<!-- Mainly scripts -->
<script src="{{ asset('vendor/inspinia/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/demo/peity-demo.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>

<!-- Data picker -->
<script src="{{ asset('vendor/inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('vendor/inspinia/js/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-confirm/jquery-confirm.min.js') }}"></script>

<!-- PNotify -->
<script src="{{ asset('vendor/pnotify/dist/pnotify.js') }}"></script>
<script src="{{ asset('vendor/pnotify/dist/pnotify.buttons.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('vendor/inspinia/js/inspinia.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ asset('vendor/inspinia/js/plugins/toastr/toastr.min.js') }}"></script>

<!-- More Scripts -->
@yield("scripts")

<!-- Custom -->
<script src="{{ asset('js/bundle.js') }}"></script>

@include('elements.toastr_message')

</body>
</html>
