@extends('backend.layout.default')

@section('title_for_layout')
    <title>Users Management</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý quyền hạn</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.permissions.index") }}">Quyền hạn</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách quyền hạn</h5>
                        <div class="ibox-tools">
                            <a href="{{ route("admin.permissions.create") }}" class="btn btn-primary btn-xs">
                                @lang("backend/common.button.add")
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover data-table table__management table__permission-management" >
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th class="text-center">@lang('backend/table.common.updated_at')</th>
                                    <th class="text-center">@lang('backend/table.common.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $key => $permission)
                                    <tr class="gradeX">
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td><a href="{{ route('admin.permissions.edit', $permission->id) }}"> {{ $permission->name }} </a></td>
                                        <td class="text-center">{{ $permission->updated_at->diffForHumans() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.permissions.edit', $permission->id) }}"
                                               class="btn btn-info btn-xs tbl__btn-edit"><i class="fa fa-edit"></i></a>
                                            <form action="{{ route('admin.permissions.destroy', $permission->id) }}"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <a class="btn btn-danger btn-xs tbl__btn-delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection