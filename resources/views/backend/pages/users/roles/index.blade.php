@extends('backend.layout.default')

@section('title_for_layout')
    <title>Users Management</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý vai trò</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.roles.index") }}">Vai trò</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách vai trò</h5>
                        <div class="ibox-tools">
                            <a href="{{ route("admin.permissions.create") }}" class="btn btn-primary btn-xs">
                                @lang("backend/common.button.add")
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover data-table table__management table__role-management" >
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th class="text-center">@lang('backend/table.common.updated_at')</th>
                                    <th class="text-center">@lang('backend/table.common.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $role)
                                    @if($role->id != AppUtil::SUPER_ADMIN_ROLE_ID)
                                    <tr class="gradeX">
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td> {{ $role->name }} </td>
                                        <td class="text-center">{{ $role->updated_at->diffForHumans() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.permissions.edit', $role->id) }}"
                                               class="btn btn-info btn-xs tbl__btn-edit"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-xs tbl__btn-delete"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection