@extends('backend.layout.default')

@section('title_for_layout')
    <title>Vai trò</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý vai trò</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.roles.index") }}">Vai trò</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <form id="frmPermission" action="{{ route('admin.roles.store') }}"
              class="form-label-left"
              method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Thông tin quyền hạn</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="form-group">
                                <label for="address">
                                    Name <span class="required">*</span>
                                </label>
                                <input type="text" id="name" name="name" maxlength="255"
                                       class="form-control" value="" required>
                            </div>
                        </div>
                    </div>
                    <div class="no-padding">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> @lang('backend/common.button.save')
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection