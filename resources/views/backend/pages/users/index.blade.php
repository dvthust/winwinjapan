@extends('backend.layout.default')

@section('title_for_layout')
    <title>Users Management</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý người dùng</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.users.index") }}">Người dùng</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách người dùng</h5>
                        <div class="ibox-tools">
                            <a href="{{ route("admin.users.create") }}" class="btn btn-primary btn-xs">
                                @lang("backend/common.button.add")
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover data-table table__management table__user-management" >
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>@lang('backend/table.user.role.title')</th>
                                    <th class="text-center">@lang('backend/table.user.avatar')</th>
                                    <th>@lang('backend/table.user.name')</th>
                                    <th>@lang('backend/table.user.phone')</th>
                                    <th>@lang('backend/table.user.email')</th>
                                    <th class="text-center">@lang('backend/table.common.updated_at')</th>
                                    <th class="text-center">@lang('backend/table.common.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $user)
                                <tr class="gradeX" data-user_id="{{ $user->id }}">
                                    <td class="text-center">{{ $key + 1 }}</td>
                                    <td style="line-height: 1.7;">
                                        @foreach ($user->roles()->pluck('name', "id") as $role_id => $role)
                                            <span class="label label-{{ $role_id == AppUtil::SUPER_ADMIN_ROLE_ID ? "danger" : "success" }} label-many">{{ $role }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.users.profile', $user->id) }}"
                                            class="user-avatar">
                                            @if(isset($user->avatar) && !empty($user->avatar))
                                                <img src="{{ $user->avatar }}">
                                            @else
                                                <img src="{{ asset('images/avatar_default.jpg') }}"
                                                     alt="">
                                            @endif
                                        </a>
                                    </td>
                                    <td class="center"><a href="{{ route('admin.users.profile', $user->id) }}"></a>{{ $user->name }}</td>
                                    <td class="center">{{ $user->phone }}</td>
                                    <td class="center">{{ $user->email }}</td>
                                    <td class="text-center">{{ $user->updated_at->diffForHumans() }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.users.profile', $user->id) }}"
                                           class="btn btn-info btn-xs tbl__btn-edit"><i class="fa fa-edit"></i></a>

                                        @if(!$user->hasRole(AppUtil::SUPER_ADMIN_ROLE_NAME))
                                            <form action="{{ route('admin.users.destroy', $user->id) }}"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <a class="btn btn-danger btn-xs tbl__btn-delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection