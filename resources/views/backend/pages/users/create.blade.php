@extends('backend.layout.default')

@section('title_for_layout')
    <title>Profile</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tài khoản</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Người dùng</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin tài khoản</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="frmUserProfile" action="{{ route('admin.users.store') }}"
                              class="form-label-left"
                              method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                    <div class="row">
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label for="name">
                                                @lang('backend/table.user.name') <span class="required">*</span>
                                            </label>
                                            <input type="text" value="" placeholder=""
                                                   id="name" name="name"
                                                   maxlength="255" class="form-control"
                                                   autofocus required="required">
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label for="gender">@lang('backend/table.user.gender.title')</label>
                                            <fieldset class="gender-radio">
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioMale" value="1" name="gender"
                                                           checked>
                                                    <label for="inlineRadioMale"> @lang('backend/table.user.gender.male') </label>
                                                </div>
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioFemale" value="2" name="gender">
                                                    <label for="inlineRadioFemale"> @lang('backend/table.user.gender.female') </label>
                                                </div>
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioOther" value="3" name="gender">
                                                    <label for="inlineRadioOther"> @lang('backend/table.user.gender.other') </label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-sm-6 col-xs-12">
                                            <label for="birthday">
                                                @lang('backend/table.user.birthday')
                                            </label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input name="birthday" type="text"
                                                       class="form-control input_date_picker"
                                                       value="">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6 col-xs-12">
                                            <label for="phone">
                                                @lang('backend/table.user.phone')
                                            </label>
                                            <input type="text" value="" id="phone"
                                                   name="phone" maxlength="45"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="email">
                                                    @lang('backend/table.user.email') <span class="required">*</span>
                                                </label>
                                                <input type="email" id="email"
                                                       value="" name="email"
                                                       placeholder="abc@email.com" maxlength="255" class="form-control"
                                                       required>
                                            </div>

                                            <div class="form-group">
                                                <label for="address">
                                                    @lang('backend/table.user.address')
                                                </label>
                                                <input type="text" id="address" name="address" maxlength="255"
                                                       class="form-control" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="about">
                                                    @lang('backend/table.user.about')
                                                </label>
                                                <textarea id="about" class="form-control" name="about"
                                                          rows="5"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">
                                                    @lang('backend/table.user.password') <span class="required">*</span>
                                                </label>
                                                <input type="password" value="" id="password" name="password"
                                                       minlength="6" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password_confirmation">
                                                    @lang('backend/table.user.password_confirmation') <span
                                                            class="required">*</span>
                                                </label>
                                                <input type="password" value="" id="password_confirmation"
                                                       name="password_confirmation"
                                                       minlength="6" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="role">@lang('backend/table.user.role.title')</label>
                                                <div class="role_list">
                                                    @foreach($roles as $role)
                                                        <div class="checkbox role_item checkbox-{{ $role->id == AppUtil::ADMINISTRATOR_ROLE_ID ? "danger" : "success" }}">
                                                            <input name="roles[]" type="checkbox"
                                                                   id="inlineCheckbox_{{$role->id}}"
                                                                   value="{{ $role->name }}">
                                                            <label for="inlineCheckbox_{{$role->id}}"> {{ $role->name }} </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="ln_solid"></div>
                                            <div class="form-group no-padding">
                                                <button type="submit" class="btn btn-success"><i
                                                            class="fa fa-save"></i> @lang('backend/common.button.save')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

