@extends('backend.layout.default')

@section('title_for_layout')
    <title>Profile</title>
@endsection

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Người dùng</a>
                </li>
                <li class="active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profile Detail</h5>
                    </div>
                    <div>
                        <div class="ibox-content no-padding border-left-right">
                            <img alt="image" class="img-responsive"
                                 src="{{ asset("vendor/inspinia/img/profile_big.jpg") }}">
                        </div>
                        <div class="ibox-content profile-content app__profile-content">
                            <h4><strong>{{ $user->name }}</strong></h4>
                            <p><i class="fa fa-map-marker"></i> {{ $user->address }} </p>
                            <h5>
                                About me
                            </h5>
                            <p>
                                {{ $user->about }}
                            </p>
                            <div class="row m-t-lg profile-detail__statistic">
                                <div class="col-md-4">
                                    <span class="bar">5,3,9,6,5,9,7,3,5,2</span>
                                    <h5><strong>169</strong> Posts</h5>
                                </div>
                                <div class="col-md-4">
                                    <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    <h5><strong>28</strong> Following</h5>
                                </div>
                                <div class="col-md-4">
                                    <span class="bar">5,3,2,-1,-3,-2,2,3,5,2</span>
                                    <h5><strong>240</strong> Followers</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1">Thông tin cá nhân</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Thông tin tài khoản</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-personal-info tab-pane active">
                            <div class="panel-body">
                                <form id="frmUserProfile" action="{{ route('admin.users.profile.update', $user->id ) }}"
                                      class="form-label-left"
                                      method="post">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label for="name">
                                                @lang('backend/table.user.name') <span class="required">*</span>
                                            </label>
                                            <input type="text" value="{{ $user->name }}" placeholder=""
                                                   id="name" name="name"
                                                   maxlength="255" class="form-control"
                                                   autofocus required="required">
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label for="gender">@lang('backend/table.user.gender.title')</label>
                                            <fieldset class="gender-radio">
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioMale" value="1"
                                                           name="gender" {{ isset($user) && $user->gender == '1' ? 'checked' : ''  }}>
                                                    <label for="inlineRadioMale"> @lang('backend/table.user.gender.male') </label>
                                                </div>
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioFemale" value="2"
                                                           name="gender" {{ isset($user) && $user->gender == '2' ? 'checked' : ''  }}>
                                                    <label for="inlineRadioFemale"> @lang('backend/table.user.gender.female') </label>
                                                </div>
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" id="inlineRadioOther" value="3"
                                                           name="gender" {{ isset($user) && $user->gender == '3' ? 'checked' : ''  }}>
                                                    <label for="inlineRadioOther"> @lang('backend/table.user.gender.other') </label>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6 col-xs-12">
                                            <label for="birthday">
                                                @lang('backend/table.user.birthday')
                                            </label>
                                            <div class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                                <input name="birthday" type="text"
                                                       class="form-control input_date_picker"
                                                       value="{{ date('d/m/Y', strtotime($user->birthday)) }}">
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-6 col-xs-12">
                                            <label for="phone">
                                                @lang('backend/table.user.phone')
                                            </label>
                                            <input type="text" value="{{ $user->phone }}" id="phone"
                                                   name="phone" maxlength="45"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">
                                            @lang('backend/table.user.email') <span class="required">*</span>
                                        </label>
                                        <input type="email" id="email"
                                               value="{{ $user->email }}"
                                               placeholder="abc@email.com" maxlength="255" class="form-control"
                                               disabled>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">
                                            @lang('backend/table.user.address')
                                        </label>
                                        <input type="text" id="address" name="address" maxlength="255"
                                               class="form-control" value="{{ $user->address }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="about">
                                            @lang('backend/table.user.about')
                                        </label>
                                        <textarea id="about" class="form-control" name="about"
                                                  rows="5">{{ $user->about }}</textarea>
                                    </div>
                                    <div class="col-xs-12 no-padding">
                                        <button type="submit" class="btn btn-success"><i
                                                    class="fa fa-save"></i> @lang('backend/common.button.save')
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-account-info tab-pane">
                            <div class="panel-body">
                                <form id="frmUserAccount" action="{{ route('admin.users.password.update') }}"
                                      class="form-label-left" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                                    <div class="form-group">
                                        <label for="password">
                                            @lang('backend/table.user.password') <span class="required">*</span>
                                        </label>
                                        <input type="password" value="" id="password" name="password"
                                               minlength="6" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">
                                            @lang('backend/table.user.password_confirmation') <span
                                                    class="required">*</span>
                                        </label>
                                        <input type="password" value="" id="password_confirmation"
                                               name="password_confirmation"
                                               minlength="6" class="form-control" required>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="col-xs-12 no-padding">
                                        <button type="submit" class="btn btn-success"><i
                                                    class="fa fa-save"></i> @lang('backend/common.button.save')
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

