@extends('backend.layout.default')

@section('title_for_layout')
    <title>Ảnh slide</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Thiết lập ảnh slider</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.settings.slider.index") }}">Ảnh Slide</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Danh sách</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table__management tbl-post-category">
                                <thead>
                                <tr>
                                    <th>
                                        <input class="i-checks check-all" type="checkbox">
                                    </th>
                                    <th> #</th>
                                    <th> Ảnh</th>
                                    <th> Tiêu đề</th>
                                    <th> Trạng thái</th>
                                    <th> @lang('backend/table.common.updated_at') </th>
                                    <th><span class="nobr"> @lang('backend/table.common.action') </span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sliders as $key => $slider)
                                    <tr class="even pointer" data-id="{{ $slider->id }}" data-model="Slideshow">
                                        <td>
                                            <input class="i-checks input_record" type="checkbox" name="records[]">
                                        </td>
                                        <td>
                                            {{ ($sliders->currentPage() - 1) * $sliders->perPage() + $key + 1 }}
                                        </td>
                                        <td> <img href="{{ $slider->link }}" src="{{ $slider->image }}" style="max-width: 100px; max-height: 100px;"> </td>
                                        <td> <a href="{{ $slider->link ? $slider->link : "" }}">{{ $slider->name }}</a> </td>
                                        <td>
                                            <input class="btn-tbl-status" type="checkbox" name="status"
                                                   {{ $slider->status == AppUtil::STATUS_ACTIVE ? "checked" : "" }} data-toggle="toggle"
                                                   data-size="mini" data-on="Active" data-off="Blocked"
                                                   data-onstyle="success" data-offstyle="danger"/>
                                        </td>
                                        <td> {{ $slider->updated_at->diffForHumans() }} </td>
                                        <td>
                                            <a href="{{ route('admin.settings.slider.edit', $slider->id) }}"
                                               class="btn btn-info btn-xs tbl__btn-edit" title="Chi tiết"><i class="fa fa-eye"></i></a>
                                            <form action="{{ route('admin.settings.slider.delete', $slider->id) }}"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <a class="btn btn-danger btn-xs tbl__btn-delete"><i
                                                            class="fa fa-trash"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="tbl-footer">
                            <div class="tbl-footer--left">
                                <form class="tbl__frm-delete--multi" action="{{ route('admin.settings.slider.delete.multi') }}"
                                      style="display: inline;" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="data_ids" value="" class="data_ids">
                                    <button type="button" class="btn btn-danger btn-sm tbl__btn-delete--multi">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <a href="{{ route('admin.settings.slider.create') }}">
                                    <button class="btn btn-primary btn-sm tbl__btn-add">
                                        <i class="fa fa-plus"></i> @lang('backend/table.common.add_new')
                                    </button>
                                </a>
                            </div>
                            <div class="pagination-container pull-right no-margin">
                                {{ $sliders->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection