@extends('backend.layout.default')

@section('title_for_layout')
    <title>Media Management</title>
@endsection

@section('content')
    <div class="media-management" style="padding: 5px 0 40px 0;">
        <iframe id="ckfinder" src="{{ asset('vendor/ckfinder/ckfinder.html?gallery=0&langCode=vi') }}" frameborder="0" style="width: 100%; min-height: 600px"></iframe>
    </div>
@endsection