@extends('backend.layout.default')

@section('title_for_layout')
    <title>Danh mục Order</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý Order</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.orders.index") }}">Bài viết</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin Order</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form action="{{ route("admin.orders.index") }}" method="GET">
                                <div class="col-sm-5 m-b-xs">
                                    <select class="input-sm form-control input-s-sm inline" name="catalogue_id">
                                        <option value="0">--Tất cả danh mục--</option>
                                        @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ app('request')->input('catalogue_id') == $category->id ? "selected" : "" }}>{{ $category->name_vi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_TO_DAY ? "active" : "" }}">
                                            <input type="radio"
                                                   id="day"
                                                   name="time"
                                                   {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_TO_DAY ? "checked" : "" }}
                                                   value="{{ \App\Eloquent\Order::$FILTER_TIME_TYPE_TO_DAY }}"> Day </label>
                                        <label class="btn btn-sm btn-white {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_WEEK ? "active" : "" }}">
                                            <input type="radio"
                                                   id="week"
                                                   name="time"
                                                   {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_WEEK ? "checked" : "" }}
                                                   value="{{ \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_WEEK }}"> Week </label>
                                        <label class="btn btn-sm btn-white {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_MONTH ? "active" : "" }}">
                                            <input type="radio"
                                                   id="month"
                                                   name="time"
                                                   {{ app('request')->input('time') == \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_MONTH ? "checked" : "" }}
                                                   value="{{ \App\Eloquent\Order::$FILTER_TIME_TYPE_THIS_MONTH }}"> Month </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text"
                                               placeholder="Tên"
                                               class="input-sm form-control"
                                               value="{{ app('request')->input('title') }}"
                                               name="title">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table__management tbl-post-category">
                                <thead>
                                <tr>
                                    <th>
                                        <input class="i-checks check-all" type="checkbox">
                                    </th>
                                    <th> #</th>
                                    <th> Tên
                                    </th>
                                    <th> Trạng thái
                                    </th>
                                    <th> @lang('backend/table.common.updated_at') </th>
                                    <th><span class="nobr"> @lang('backend/table.common.action') </span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $key => $post)
                                    <tr class="even pointer" data-id="{{ $post->id }}" data-model="Order">
                                        <td><input type="checkbox" class="i-checks input_record" name="records[]"></td>
                                        <td>
                                            {{ ($posts->currentPage() - 1) * $posts->perPage() + $key + 1 }}
                                        </td>
                                        <td><a href="{{ route('admin.orders.edit', $post->id) }}"> {{ $post->name_vi }} </a></td>
                                        <td class="tbl__btn-status">
                                            <input class="btn-tbl-status" type="checkbox" name="status"
                                                   {{ $post->status == AppUtil::STATUS_ACTIVE ? "checked" : "" }} data-toggle="toggle"
                                                   data-size="mini" data-on="Active" data-off="Blocked"
                                                   data-onstyle="primary" data-offstyle="danger"/>
                                        </td>
                                        <td> {{ $post->updated_at->diffForHumans() }} </td>
                                        <td>
                                            <a href="{{ route('admin.orders.edit', $post->id) }}"
                                               class="btn btn-info btn-xs tbl__btn-edit" title="Chi tiết"><i
                                                        class="fa fa-edit"></i></a>
                                            <form action="{{ route('admin.orders.destroy', $post->id) }}"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <a class="btn btn-danger btn-xs tbl__btn-delete"><i
                                                            class="fa fa-trash"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tbl-footer">
                            <div class="tbl-footer--left">
                                <form class="tbl__frm-delete--multi" action="{{ route('admin.orders.delete.multi') }}"
                                      style="display: inline;" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="data_ids" value="" class="data_ids">
                                    <button type="button" class="btn btn-danger btn-sm tbl__btn-delete--multi">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <a href="{{ route('admin.orders.create') }}">
                                    <button class="btn btn-primary btn-sm tbl__btn-add">
                                        <i class="fa fa-plus"></i> @lang('backend/table.common.add_new')
                                    </button>
                                </a>
                            </div>
                            <div class="pagination-container pull-right no-margin">
                                {{ $posts->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection