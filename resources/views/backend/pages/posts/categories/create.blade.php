@extends('backend.layout.default')

@section('title_for_layout')
    <title>Quản lý danh mục</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý danh mục</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.categories.index") }}">Danh mục</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin danh mục</h5>
                        <div class="pull-right">
                            <div id="language-group" class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle btn--language" data-toggle="dropdown" aria-expanded="false">
                                    <span class="language--span">Vietnamese</span>
                                    <i class="caret"></i>
                                </button>
                                <ul class="dropdown-menu pull-right language-list" role="menu">
                                    <li data-lang="vi" class="lang--option active"><a href="#">Vietnamese</a></li>
                                    <li data-lang="ja" class="lang--option"><a href="#">Japanese</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="hhv-post-category" class="form-horizontal form-label-left"
                              action="{{ route('admin.categories.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tên danh mục
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"
                                           class="form-control input-language" data-lang="vi" name="name_vi" value="">
                                    <input type="text"
                                           class="form-control input-language hidden" data-lang="ja" name="name_ja" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Mô tả
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control input-language" name="description_vi" data-lang="vi"
                                                  rows="5"></textarea>
                                    <textarea class="form-control input-language hidden" name="description_ja" data-lang="ja"
                                              rows="5"></textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                    <a href="{{ route('admin.categories.index') }}"><button type="button" class="btn btn-danger">Huỷ</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
