@extends('backend.layout.default')

@section('title_for_layout')
    <title>Quản lý thẻ tag</title>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý thẻ tag</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route("admin.tags.index") }}">Danh mục</a>
                </li>
                <li class="active">
                    <strong>Chỉnh sửa</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin thẻ tag</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="app-tag" class="form-horizontal form-label-left" action="{{ route('admin.tags.update', $item->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field("PUT") }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tên thẻ tag
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="name" required="required"
                                           class="form-control" name="name" value="{{ $item->name }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Mô tả
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="description" class="form-control" name="description"
                                                  rows="5">{{ $item->description }}</textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                    <a href="{{ route('admin.tags.index') }}"><button type="button" class="btn btn-danger">Huỷ</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
