@extends('backend.layout.default')

@section('title_for_layout')
    <title>405 Page Not Found Error</title>
@endsection

@section('content')
    <div class="container">
        <h1>Sorry, Page Not Found 404</h1>
    </div>
@endsection