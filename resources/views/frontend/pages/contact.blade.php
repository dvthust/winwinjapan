@extends('frontend.layout.default')

@section('title_for_layout')
    <title>@lang('frontend/common.header.contact')</title>
@endsection

@section('content')
    <div class="row"><!--Container row-->
        <div class="span8 contact"><!--Begin page content column-->
            <h2>@lang('frontend/common.header.contact')</h2>

            @include('elements.contact_msg')

            <form action="{{ route('contact.post')  }}" id="contact-form" method="post">
                {{ csrf_field() }}
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input class="span6" id="prependedInput" size="16" type="text" placeholder="@lang('frontend/page.contact.name')" name="name" required>
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input class="span6" id="prependedInput" size="16" type="text" placeholder="@lang('frontend/page.contact.email')" name="email">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-globe"></i></span>
                    <input class="span6" id="prependedInput" size="16" type="text" placeholder="@lang('frontend/page.contact.phone')" name="phone">
                </div>
                <textarea class="span7" name="content"></textarea>
                <div class="row">
                    <div class="span2">
                        <input type="submit" class="btn btn-inverse" value="@lang('frontend/page.contact.send_btn')">
                    </div>
                </div>
            </form>

        </div> <!--End page content column-->

        <!-- Sidebar
        ================================================== -->
        <div class="span4 sidebar page-sidebar"><!-- Begin sidebar column -->
            <h5 class="title-bg">@lang('frontend/page.contact.contact_info')</h5>
            <address>
                <strong>@lang('frontend/page.contact.company_name')</strong><br>
                <address>
                    <strong>@lang('frontend/page.contact.address'):</strong> @lang('frontend/page.contact.address_detail')<br>
                    <strong>@lang('frontend/page.contact.email'):</strong> @lang('frontend/page.contact.email_detail')<br>
                    <strong>@lang('frontend/page.contact.phone'):</strong> @lang('frontend/page.contact.phone_detail')
                </address>
            </address>

            <h5 class="title-bg">@lang('frontend/page.contact.map')</h5>
            <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d930.9632965779775!2d105.76533447918797!3d21.03855959912459!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e2!4m5!1s0x313454c760d128c3%3A0x626896083e77fa43!2zxJDGsOG7nW5nIExpw6puIEPGoSwgQ-G6p3UgRGnhu4VuLCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!3m2!1d21.0389831!2d105.7662088!4m3!3m2!1d21.0381991!2d105.7655829!5e0!3m2!1svi!2s!4v1509261625029" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div><!-- End sidebar column -->

    </div><!-- End container row -->
@endsection
