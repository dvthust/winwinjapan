@extends('frontend.layout.default')

@section('title_for_layout')
    <title>@lang('frontend/common.header.home')</title>
@endsection

@section('content')
    <div class="row headline"><!-- Begin Headline -->

        <!-- Slider Carousel
       ================================================== -->
    @include("frontend.elements.banner")

    <!-- Headline Text
        ================================================== -->
        <div class="span4">
            <h3>Welcome to Piccolo. <br />
                A Big Theme in a Small Package.</h3>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium vulputate magna sit amet blandit.</p>
            <p>Cras rutrum, massa non blandit convallis, est lacus gravida enim, eu fermentum ligula orci et tortor. In sit amet nisl ac leo pulvinar molestie. Morbi blandit ultricies ultrices.</p>
            <a href="#"><i class="icon-plus-sign"></i>Chi tiết</a>
        </div>
    </div><!-- End Headline -->

    <div class="row"><!--Container row-->
        <!-- Page Sidebar
        ================================================== -->
        <div class="span3 sidebar page-sidebar"><!-- Begin sidebar column -->
            <!--Goc Tu Van-->
            <h5 class="title-bg" style="margin-top: 0px;">@lang('frontend/common.header.consultant')</h5>
            <ul class="popular-posts">
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Nulla iaculis mattis lorem, quis gravida nunc iaculis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li><li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum maecenas felis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
            </ul>

            <!--Ky su co khi-->
            <h5 class="title-bg" style="margin-top: 0px;">@lang('frontend/common.header.engineer_mechanical')</h5>
            <ul class="popular-posts">
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Nulla iaculis mattis lorem, quis gravida nunc iaculis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li><li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum maecenas felis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
            </ul>

        </div><!-- End sidebar column -->

        <!-- Page Content
        ================================================== -->
        <div class="span9 blog">

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">A subject that is beautiful in itself</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                <button class="btn btn-mini btn-inverse" type="button">Read more</button>
                <div class="post-summary-footer">
                    <ul class="post-data-3">
                        <li><i class="icon-calendar"></i> 09/04/15</li>
                        <li><i class="icon-user"></i> <a href="#">Admin</a></li>
                        <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>
                        <li><i class="icon-tags"></i> <a href="#">photoshop</a>, <a href="#">tutorials</a></li>
                    </ul>
                </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">A great artist is always before his time</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                <button class="btn btn-mini btn-inverse" type="button">Read more</button>
                <div class="post-summary-footer">
                    <ul class="post-data-3">
                        <li><i class="icon-calendar"></i> 09/04/15</li>
                        <li><i class="icon-user"></i> <a href="#">Admin</a></li>
                        <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>
                        <li><i class="icon-tags"></i> <a href="#">photoshop</a>, <a href="#">tutorials</a></li>
                    </ul>
                </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">Is art everything to anybody?</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                <button class="btn btn-mini btn-inverse" type="button">Read more</button>
                <div class="post-summary-footer">
                    <ul class="post-data-3">
                        <li><i class="icon-calendar"></i> 09/04/15</li>
                        <li><i class="icon-user"></i> <a href="#">Admin</a></li>
                        <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>
                        <li><i class="icon-tags"></i> <a href="#">photoshop</a>, <a href="#">tutorials</a></li>
                    </ul>
                </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">A Brand New Illustration Contest</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                <button class="btn btn-mini btn-inverse" type="button">Read more</button>
                <div class="post-summary-footer">
                    <ul class="post-data-3">
                        <li><i class="icon-calendar"></i> 09/04/15</li>
                        <li><i class="icon-user"></i> <a href="#">Admin</a></li>
                        <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>
                        <li><i class="icon-tags"></i> <a href="#">photoshop</a>, <a href="#">tutorials</a></li>
                    </ul>
                </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">How to Create a Beautiful Scene</a></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                <button class="btn btn-mini btn-inverse" type="button">Read more</button>
                <div class="post-summary-footer">
                    <ul class="post-data-3">
                        <li><i class="icon-calendar"></i> 09/04/15</li>
                        <li><i class="icon-user"></i> <a href="#">Admin</a></li>
                        <li><i class="icon-comment"></i> <a href="#">5 Comments</a></li>
                        <li><i class="icon-tags"></i> <a href="#">photoshop</a>, <a href="#">tutorials</a></li>
                    </ul>
                </div>
            </article>

            <!-- Pagination -->
            <div class="pagination">
                <ul>
                    <li class="active"><a href="#">Prev</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection
