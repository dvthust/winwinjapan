<div class="row header"><!-- Begin Header -->

    <!-- Logo
    ================================================== -->
    <div class="span4 logo">
        <a href="{{ route('index') }}"><img src="{{ asset("images/logo.png") }}" alt="" /></a>
    </div>

    <div class="span4 slogan">
        <h4 class="slogan">@lang('frontend/common.slogan')</h4>
    </div>

    <div class="span4 language-change">
        <form class="change-lang-frm" method="POST" action="{{ route("changelocale") }}">
            {{ csrf_field() }}
            @if(App::isLocale('vi'))
                <input type="hidden" name="lang" value="ja">
                <a class="language-item ja-lang" href="#"><img src="{{ asset('img/flag_ja.png') }}"></a>
            @else
                <input type="hidden" name="lang" value="vi">
                <a class="language-item ja-lang" href="#"><img src="{{ asset('img/flag_vn.png') }}"></a>
            @endif
        </form>
    </div>

    <!-- Main Navigation
    ================================================== -->
    <div class="span12 navigation winwin__navigation">
        <div class="navbar hidden-phone">

            <ul class="nav">
                <li class="dropdown active">
                    <a href="{{ route('index') }}">@lang('frontend/common.header.home')</a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">@lang('frontend/common.header.introduce')<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('introduce') }}">@lang('frontend/common.header.introduce')</a></li>
                        <li><a href="{{ route('introduce.company') }}">@lang('frontend/common.header.introduce_company')</a></li>
                        <li><a href="{{ route('activities') }}">@lang('frontend/common.header.activities')</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">@lang('frontend/common.header.engineer') <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('quy-trinh-tuyen-dung-ky-su') }}">@lang('frontend/common.header.engineer_job') </a></li>
                        <li><a href="{{ url('quy-trinh-tuyen-dung-ky-su-co-khi') }}">@lang('frontend/common.header.engineer_job_mechanical') </a></li>
                        <li><a href="{{ url('quy-trinh-tuyen-dung-ky-su-it') }}">@lang('frontend/common.header.engineer_job_it') </a></li>
                    </ul>
                </li>
                <li><a href="{{ url('ky-su-co-khi') }}">@lang('frontend/common.header.engineer_mechanical')</a></li>
                <li><a href="{{ url('thuc-tap-sinh') }}">@lang('frontend/common.header.apprentice')</a></li>
                <li><a href="{{ url('du-hoc-han-quoc') }}">@lang('frontend/common.header.student_korean')</a></li>
                <li><a href="{{ url('du-hoc-nhat-ban') }}">@lang('frontend/common.header.student_japan')</a></li>
                <li><a href="{{ url('goc-tu-van') }}">@lang('frontend/common.header.consultant')</a></li>
                <li><a href="{{ route('contact') }}">@lang('frontend/common.header.contact')</a></li>
            </ul>

        </div>

        <!-- Mobile Nav
        ================================================== -->
        <form action="#" id="mobile-nav" class="visible-phone">
            <div class="mobile-nav-select">
                <select onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                    <option value="">Navigate...</option>
                    <option value="index.htm">Home</option>
                    <option value="index.htm">- Full Page</option>
                    <option value="index-gallery.htm">- Gallery Only</option>
                    <option value="index-slider.htm">- Slider Only</option>
                    <option value="features.htm">Features</option>
                    <option value="page-full-width.htm">Pages</option>
                    <option value="page-full-width.htm">- Full Width</option>
                    <option value="page-right-sidebar.htm">- Right Sidebar</option>
                    <option value="page-left-sidebar.htm">- Left Sidebar</option>
                    <option value="page-double-sidebar.htm">- Double Sidebar</option>
                    <option value="gallery-4col.htm">Gallery</option>
                    <option value="gallery-3col.htm">- 3 Column</option>
                    <option value="gallery-4col.htm">- 4 Column</option>
                    <option value="gallery-6col.htm">- 6 Column</option>
                    <option value="gallery-4col-circle.htm">- Gallery 4 Col Round</option>
                    <option value="gallery-single.htm">- Gallery Single</option>
                    <option value="blog-style1.htm">Blog</option>
                    <option value="blog-style1.htm">- Blog Style 1</option>
                    <option value="blog-style2.htm">- Blog Style 2</option>
                    <option value="blog-style3.htm">- Blog Style 3</option>
                    <option value="blog-style4.htm">- Blog Style 4</option>
                    <option value="blog-single.htm">- Blog Single</option>
                    <option value="page-contact.htm">Contact</option>
                </select>
            </div>
        </form>

    </div>
</div><!-- End Header -->