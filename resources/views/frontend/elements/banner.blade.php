<div class="span8">
    <div class="flexslider">
        <ul class="slides">
            @if(isset($banners) && $banners->count())
                @foreach($banners as $banner)
                    <li><a href="{{ $banner->link }}"><img src="{{ $banner->image }}" alt="{{ $banner->name }}" /></a></li>
                @endforeach
            @else
                <li><a href="#"><img src="{{ asset('img/tong_cong_ty.jpg') }}" /></a></li>
            @endif
        </ul>
    </div>
</div>