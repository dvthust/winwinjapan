<!-- Footer Area
        ================================================== -->

<div class="footer-container"><!-- Begin Footer -->
    <div class="container">
        <div class="row footer-row">
            <div class="span4 footer-col">
                <h5>About Us</h5>
                <img src="http://winwinjapan.vn/img/logo_footer.png" alt="HanoiLink" /><br /><br />
                <address>
                    <strong>Công Ty TNHH Hợp Tác Phát Triển Nguồn Nhân Lực WINWINJAPAN</strong><br />
                    Đường liên cơ - Từ Liêm - Hà Nội<br />
                    0977258192 - hai.xkld.vinafor@gmail.com<br />
                </address>
            </div>
            <div class="span4 footer-col">
                <h4>Văn phòng tại khu vực KANTO</h4>
                <ul>
                    <li><a>Địa chỉ:</a> 6 Chome-12-10 Tsujidō Motomachi, Fujisawa-shi, Kanagawa-ken 251-0043, Japan.</li>
                    <li><a>Điện thoại:</a> (+81) 50 5534 0799.</li>
                    <li><a>Email</a> nghia@vinafor.edu.vn</li>
                </ul>
                <h4>Văn phòng tại khu vực KYUSHU</h4>
                <ul>
                    <li><a>Địa chỉ:</a> 670 Karijuku, Ōsaki-chō, Soo-gun, Kagoshima-ken 899-7305, Japan.</li>
                    <li><a>Điện thoại:</a> (+81) 80 4628 0386.</li>
                    <li><a>Email</a> nghia@vinafor.edu.vn</li>
                </ul>
            </div>
            <div class="span4 footer-col">
                <h4>Văn Phòng Đại Diện</h4>
                <ul>
                    <li><a>VPĐD 1:</a> Chi nhánh miền Bắc</li>
                    <li><a>Địa chỉ:</a> A14 - Ngách 10 - Ngõ 3 - Đường Liên Cơ - Nam Từ Liêm  - Hà Nội</li>
                    <li><a>Điện thoại:</a> 0977258192</li>
                    <li><a>Email</a> hainx2311.winwinjapan@gmail.com</li>
                    <li>&nbsp;</li>
                </ul>
            </div>
        </div>

        <div class="row"><!-- Begin Sub Footer -->
            <div class="span12 footer-col footer-sub">
                <div class="row no-margin">
                    <div class="span6"><span class="left">2017 - Developed by <a href="http://bbstudio.vn" title="BBStudio">BBStudio.</a></span></div>
                    <div class="span6">
                            <span class="right">
                            <a href="http://winwinjapan.vn/" title="Home">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://winwinjapan.vn/gioi-thieu/" title="Giới thiệu">Giới thiệu</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://winwinjapan.vn/tuyen-dung/">Tuyển dụng</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://winwinjapan.vn/tin-tuc/" title="Tin tức">Tin tức</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="http://winwinjapan.vn/lien-he/" title="Liên hệ">Liên hệ</a>
                            </span>
                    </div>
                </div>
            </div>
        </div><!-- End Sub Footer -->

    </div>
</div><!-- End Footer -->

<!-- Scroll to Top -->
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>