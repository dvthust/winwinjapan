@extends('frontend.layout.default')

@section('title_for_layout')
    <title>404 Page Not Found Error</title>
@endsection

@section('content')
    <div class="container app__wrapper">
        <div class="row">
            <div class="login--container">
                <div class="app__content--notfound span6 col-sm-10 col-md-8 offset-sm-1 offset-md-2">
                    <div class="app__box--general">
                        <div class="notfound__title">
                            <h1 class="title">Page not found</h1>
                        </div>
                        <div class="notfound__content">
                            <p>The page that you are looking for does not exist on this website. You may have accidentally mistype the page address, or followed an expired link. Anyway, we will help you get back on track. Why not try to search for the page you were looking for:</p>
                            <form class="notfound__search-form" action="#" method="get">
                                <div class="row notfound__search--box no-gutters">
                                    <input class="span4 form-control col-12 col-md-8" name="s" type="text" value="" placeholder="Type here to search..." style="margin-bottom: 0; margin-left: 30px;">
                                    <button type="submit" class="notfound__search--btn app__button btn btn-default">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
