<!-- Toastr Message -->
@if(Session::has("flash_notification"))

    @foreach (session('flash_notification', collect())->toArray() as $message)
        <div class="alert alert-{{ $message['level'] }}">
            {{--@lang('frontend/page.contact.msg_success')--}}
            {{ $message['message'] }}
        </div>
    @endforeach

    {{ session()->forget('flash_notification') }}

@endif