<!-- Toastr Message -->
@if(Session::has("flash_notification"))

<div id="toast-container" class="toast-message-custom toast-top-right">
@foreach (session('flash_notification', collect())->toArray() as $message)
    <div class="toast toast-{{ $message['level'] }}">
        <div class="toast-progress"></div>
        <button type="button" class="toast-close-button toast-close-button-custom" role="button">×</button>
        <div class="toast-title">
            {{ $message['title'] ? $message['title'] : "Thông báo" }}
        </div>
        <div class="toast-message">{{ $message['message'] }}</div>
    </div>
@endforeach
</div>

{{ session()->forget('flash_notification') }}

@endif