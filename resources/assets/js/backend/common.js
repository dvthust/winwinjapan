"use strict";

var APP = APP || {};
APP.BE = {};
APP.BE.COMMON = {};

var CURRENT_URL = window.location.href.split('?')[0],
    $SIDEBAR_MENU = $('#side-menu');

// Check active menu
// $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('active');

$SIDEBAR_MENU.find('a').filter(function () {
    return this.href == CURRENT_URL;
}).parent('li').addClass('active').parents('ul').not(".metismenu").parent().addClass('active');

/**
 * Hide Flash message after 3s
 */
$('.alert').delay(3000).hide('slow');

/**
 *PNOTIFY NOTIFICATION
 */
APP.BE.COMMON.PNOTIFY = function (text, type, custom) {
    var objPNotify = {
        text: text,
        type: typeof type != 'undefined' ? type : 'success',
        styling: 'bootstrap3'
    };
    if (typeof custom != 'undefined') {
        $.extend(objPNotify, custom);
    }
    return new PNotify(objPNotify);
};

/**
 * Ajax setup
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * Set active or deactive status of specific model
 * @param model
 * @param item_id
 * @param active
 */
APP.BE.COMMON.setActive = function (model, item_id, active) {
    var url = DOMAIN_ADMIN + '/common/setActive';
    $.ajax({
        url: url,
        type: 'post',
        data: {
            model: model,
            item_id: item_id,
            active: active
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            APP.BE.COMMON.PNOTIFY(data.data.msg);
        }
    });
};


/**
 * Set active or deactive status of specific model
 * @param model
 * @param item_id
 * @param active
 */
APP.BE.COMMON.setHighlight = function (model, item_id, active) {
    var url = DOMAIN_ADMIN + '/common/setHighlight';
    $.ajax({
        url: url,
        type: 'post',
        data: {
            model: model,
            item_id: item_id,
            active: active
        },
        dataType: 'json',
        success: function (data) {
            APP.BE.COMMON.PNOTIFY(data.data.msg);
        }
    });
};

/**
 * Delete confirmation dialog
 * @param callback
 */
APP.BE.COMMON.confirm_delete = function (callback) {
    $.confirm({
        icon: 'fa fa-warning',
        title: APP_LANG.delete_confirm_title,
        content: APP_LANG.delete_confirm,
        type: 'red',
        autoClose: 'cancel|5000',
        buttons: {
            ok: {
                text: APP_LANG.delete,
                btnClass: 'btn-danger',
                action: callback
            },
            cancel: {
                text: APP_LANG.cancle
            }
        }
    });
};

/**
 * Init ICheck
 */
$('.i-checks').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green'
});

/**
 * Check all item on table
 */
var btnCheckAll = $("input.check-all");

btnCheckAll.on('ifChecked', function(){
    $("tbody").find("input.i-checks").iCheck('check');
});

btnCheckAll.on('ifUnchecked', function(){
    $("tbody").find("input.i-checks").iCheck('uncheck');
});

//Hanlde delete multi item
$('.tbl__btn-delete--multi').on('click', function () {
    var self = $(this);
    var input_selected = $('input.input_record:checked');
    var data_ids = [];
    for (var i = 0; i < input_selected.length; ++i) {
        var data_id = $(input_selected[i]).closest('tr').data('id');
        data_ids.push(data_id);
    }
    if (data_ids.length) {
        APP.BE.COMMON.confirm_delete(function () {
            var form = self.closest('form');
            self.siblings('.data_ids').val(data_ids);
            form.submit();
        });
    }else {
        APP.BE.COMMON.PNOTIFY(APP_LANG.at_least_one, 'warning');
    }
});

//Set active status
$('.btn-tbl-status').on('change', function () {
    var item_id = $(this).closest('tr').data('id');
    var model = $(this).closest('tr').data('model');
    var active = $(this).prop('checked');

    if (item_id && !isNaN(item_id)) {
        APP.BE.COMMON.setActive(model, item_id, active);
    }
});

//Set active highlight
$('.btn-tbl-highlight').on('change', function () {
    var item_id = $(this).closest('tr').data('id');
    var model = $(this).closest('tr').data('model');
    var active = $(this).prop('checked');

    if (item_id && !isNaN(item_id)) {
        APP.BE.COMMON.setHighlight(model, item_id, active);
    }
});