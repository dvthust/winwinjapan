$(function () {

    $('input.input_date_picker').datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('table.data-table').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });

    //Flash Meassage
    $('div.toast-message-custom').delay(3000).fadeOut(350);
    $('button.toast-close-button-custom').on("click", function () {
        $(this).closest(".toast-message-custom").remove();
    });

    //Confirm before delete
    $('.table__management').on('click', '.tbl__btn-delete', function () {
        var self = $(this);
        APP.BE.COMMON.confirm_delete(function () {
            self.closest('.tbl__frm-delete').submit();
        });
    });

    //Handle click change language
    $(".lang--option").on("click", function (e) {
        e.preventDefault();
        var self = $(this);
        var lang = self.data("lang");

        self.siblings().removeClass("active");
        self.addClass("active");

        $(".language--span").text(self.find('a').text());
        $(".input-language").not("[data-lang=" + lang + "]").addClass("hidden");
        $(".input-language[data-lang=" + lang + "]").removeClass("hidden");
    });

    //Highlight checkbox post
    $("input.ckHighlight").on('ifChecked', function(){
        $(".highlight--hidden").val(1);
    });
    $("input.ckHighlight").on('ifUnchecked', function(){
        $(".highlight--hidden").val(0);
    });
});