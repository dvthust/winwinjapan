"use strict";

var APP = APP || {};
APP.FE = {};
APP.FE.COMMON = {};

//Application in debug or production mode
APP.FE.COMMON.DEBUG = true;
APP.FE.DISABLE_LOG = function () {
    if(!APP.FE.COMMON.DEBUG){
        var noop = function () {};
        console.log = console.warn = console.error = console.info = noop;
    }
};
APP.FE.DISABLE_LOG();

//