<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <?php echo $__env->yieldContent('title_for_layout'); ?>

    <!-- CSS
    ================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/prettyPhoto.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/flexslider.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/custom-styles.css')); ?>">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" href="<?php echo e(asset('css/style-ie.css')); ?>"/>
    <![endif]-->

    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.png')); ?>">
    <link rel="apple-touch-icon" href="<?php echo e(asset('img/apple-touch-icon.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('img/apple-touch-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('img/apple-touch-icon-114x114.png')); ?>">


    <!-- JS
    ================================================== -->
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="<?php echo e(asset('js/bootstrap.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.prettyPhoto.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.flexslider.js')); ?>"></script>
    <script src="<?php echo e(asset('js/jquery.custom.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#btn-blog-next").click(function () {
                $('#blogCarousel').carousel('next')
            });
            $("#btn-blog-prev").click(function () {
                $('#blogCarousel').carousel('prev')
            });

            $("#btn-client-next").click(function () {
                $('#clientCarousel').carousel('next')
            });
            $("#btn-client-prev").click(function () {
                $('#clientCarousel').carousel('prev')
            });

        });

        $(window).load(function(){

            $('.flexslider').flexslider({
                animation: "slide",
                slideshow: true,
                start: function(slider){
                    $('body').removeClass('loading');
                }
            });
        });

    </script>

</head>


<body class="home">

<?php echo $__env->make("frontend.elements.color_bar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="container">

    <?php echo $__env->make("frontend.elements.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->yieldContent("content"); ?>

</div> <!-- End Container -->

<?php echo $__env->make("frontend.elements.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script src="<?php echo e(asset("js/all.js")); ?>"></script>
</body>
</html>
