<!-- Toastr Message -->
<?php if(Session::has("flash_notification")): ?>

<div id="toast-container" class="toast-message-custom toast-top-right">
<?php $__currentLoopData = session('flash_notification', collect())->toArray(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="toast toast-<?php echo e($message['level']); ?>">
        <div class="toast-progress"></div>
        <button type="button" class="toast-close-button toast-close-button-custom" role="button">×</button>
        <div class="toast-title">
            <?php echo e($message['title'] ? $message['title'] : "Thông báo"); ?>

        </div>
        <div class="toast-message"><?php echo e($message['message']); ?></div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php echo e(session()->forget('flash_notification')); ?>


<?php endif; ?>