<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('vendor/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset("js/ck.js")); ?>"></script>

    <script>
        CKEDITOR.replace( 'content_vi' );
        CKEDITOR.replace( 'content_ja' );
        CKEDITOR.replace( 'short_des_vi' );
        CKEDITOR.replace( 'short_des_ja' );
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title_for_layout'); ?>
    <title>Quản lý order</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý order</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Order</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin order</h5>
                        <div class="pull-right">
                            <div id="language-group" class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle btn--language" data-toggle="dropdown" aria-expanded="false">
                                    <span class="language--span">Vietnamese</span>
                                    <i class="caret"></i>
                                </button>
                                <ul class="dropdown-menu pull-right language-list" role="menu">
                                    <li data-lang="vi" class="lang--option active"><a href="#">Vietnamese</a></li>
                                    <li data-lang="ja" class="lang--option"><a href="#">Japanese</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="app-category" class="form-label-left"
                              action="<?php echo e(route('admin.orders.update', optional($order)->id)); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field("PUT")); ?>

                            <div class="form-group">
                                <label class="control-label" for="code">Mã order
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="code" value="<?php echo e(optional($order)->code); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="name">Tên order
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control input-language" name="name_vi" data-lang="vi" value="<?php echo e(optional($order)->name_vi); ?>">
                                    <input type="text" class="form-control input-language hidden" name="name_ja" data-lang="ja" value="<?php echo e(optional($order)->name_ja); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="name">Danh mục
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <select class="form-control" name="catalogue_id">
                                        <option value="<?php echo e(0); ?>">--Lựa chọn--</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($cat->id); ?>" <?php echo e($cat->id == optional($order)->catalogue_id ? " selected" : ""); ?>><?php echo e($cat->name_vi); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputImage" class="control-label">
                                    Ảnh minh hoạ</label>
                                <div class="">
                                    <div class="form-group input-group">
                                        <input type="text" id="browseServer-image" name="image" class="form-control"
                                               value="<?php echo e(optional($order)->image); ?>" placeholder="Nhấp chọn ảnh"
                                               onclick="selectFileWithCKFinder('browseServer-image');">
                                        <span class="input-group-btn">
                                        <button class="btn btn-danger" type="button" onclick="delSingleImg(this);"><i
                                                    class="fa fa-minus"></i>
                                        </button>
                                             <button class="btn btn-default" type="button"
                                                     data-target="browseServer-image"
                                                     onclick="showMultiImage(this);"><i class="fa fa-eye"></i>
                                             </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="video">Video Youtube
                                </label>
                                <div class="">
                                    <input type="text" class="form-control" name="video" value="<?php echo e(optional($order)->video); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="total">Số lượng
                                </label>
                                <div class="">
                                    <input type="number" class="form-control" name="total" value="<?php echo e(optional($order)->total); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="content"> Chi tiết Order
                                </label>
                                <div class="input-language" data-lang="vi">
                                    <textarea class="form-control" name="content_vi" rows="5"><?php echo e(optional($order)->content_vi); ?></textarea>
                                </div>
                                <div class="input-language hidden" data-lang="ja">
                                    <textarea class="form-control" name="content_ja" data-lang="ja" rows="5"><?php echo e(optional($order)->content_ja); ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="short_des">Thông tin bổ sung
                                </label>
                                <div class="input-language" data-lang="vi">
                                    <textarea class="form-control" name="short_des_vi" rows="5"><?php echo e(optional($order)->short_des_vi); ?></textarea>
                                </div>
                                <div class="input-language hidden" data-lang="ja">
                                    <textarea class="form-control" name="short_des_ja" rows="5"><?php echo e(optional($order)->short_des_ja); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="short_des">Hạn nộp hồ sơ
                                </label>

                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control input_date_picker" name="exp_date" value="<?php echo e(date("m/d/Y", strtotime($order->exp_date))); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="short_des">Ngày thi
                                </label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="exam_date" class="form-control input_date_picker" value="<?php echo e(date("m/d/Y", strtotime($order->exam_date))); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_title">Title Seo</label>
                                <div class="">
                                    <textarea id="meta_title" class="form-control" name="meta_title" rows="3"><?php echo e(optional($order)->meta_title); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_key">Meta keyword</label>
                                <div class="">
                                    <textarea id="meta_key" class="form-control" name="meta_keyword" rows="3"><?php echo e(optional($order)->meta_keyword); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_des">Meta description</label>
                                <div class="">
                                    <textarea id="meta_des" class="form-control" name="meta_description" rows="3"><?php echo e(optional($order)->meta_description); ?></textarea>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                    <a href="<?php echo e(route('admin.orders.index')); ?>"><button type="button" class="btn btn-danger">Huỷ</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- My Modal Image -->
    <div id="myModalImage" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <img src="" class="image_preview img-responsive center-block">
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>