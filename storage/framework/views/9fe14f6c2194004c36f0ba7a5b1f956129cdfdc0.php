<?php $__env->startSection('title_for_layout'); ?>
    <title>Quản lý chi nhánh</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý chi nhánh</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route("admin.supports.index")); ?>">Danh mục</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin chi nhánh</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="app-tag" class="form-horizontal form-label-left"
                              action="<?php echo e(route('admin.supports.store')); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tên
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="name" required="required" class="form-control" name="name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="email" class="form-control" name="email" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Địa chỉ
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="address" class="form-control" name="address" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Số điện thoại</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="telephone" class="form-control" name="telephone" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="position">Vị trí</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="position" class="form-control" name="position" value="">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                    <a href="<?php echo e(route('admin.supports.index')); ?>"><button type="button" class="btn btn-danger">Huỷ</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>