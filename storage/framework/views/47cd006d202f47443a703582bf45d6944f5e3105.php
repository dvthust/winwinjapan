<?php $__env->startSection('content'); ?>

    <div class="row login--container">
        <div class="span6">
            <!--User Login-->
            <h5 class="title-bg" style="margin-top: 0px;">User Login</h5>
            <form class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
                <?php echo e(csrf_field()); ?>

                <div class="input-prepend <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                <span class="add-on">
                    <i class="icon-user"></i></span>
                    <input class="span6" id="prependedInput" size="16" type="text" placeholder="Địa chỉ email" name="email" value="<?php echo e(old('email')); ?>">
                </div>
                <?php if($errors->has('email')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                <?php endif; ?>
                <div class="input-prepend <?php echo e($errors->has('password') ? ' has-error' : ''); ?>" style="margin: 15px 0 10px 0;">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input class="span6" id="appendedPrependedInput" size="16" type="password" placeholder="Mật khẩu" name="password">
                </div>
                <?php if($errors->has('password')): ?>
                    <span class="help-block pwd-error">
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                <?php endif; ?>
                <div class="login__remember-me">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Remember Me
                        </label>
                    </div>
                </div>
                <div class="login__btn--container">
                    <button class="btn btn-small btn-inverse" type="submit">Đăng nhập</button>
                    <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                        Quên mật khẩu
                    </a>
                </div>
            </form>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>