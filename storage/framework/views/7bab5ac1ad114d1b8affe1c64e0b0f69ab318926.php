<?php $__env->startSection('title_for_layout'); ?>
    <title>405 Page Not Found Error</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="login--container">
        <h1>Sorry, Page Not Found 405</h1>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>