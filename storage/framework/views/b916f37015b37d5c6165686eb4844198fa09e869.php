<?php $__env->startSection('title_for_layout'); ?>
    <title>Cài đặt ảnh slide</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('vendor/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset("js/ck.js")); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Thông tin ảnh</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route("admin.settings.slider.index")); ?>">Ảnh Slide</a>
                </li>
                <li class="active">
                    <strong>Chi tiết ảnh</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <form id="hhv-post-category" class="form-horizontal form-label-left"
                  action="<?php echo e(route('admin.settings.slider.store')); ?>" method="POST">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tiêu đề
                        <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="name" required="required" class="form-control" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputImage" class="control-label col-md-3 col-sm-3 col-xs-12">
                        Ảnh </label>
                    <div class="control-label col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 5px;">
                        <div class="input-group">
                            <input type="text" id="browseServer-image" name="image" class="form-control"
                                   value="" placeholder="Nhấp chọn ảnh"
                                   onclick="selectFileWithCKFinder('browseServer-image');">
                            <span class="input-group-btn">
                                        <button class="btn btn-danger" type="button" onclick="delSingleImg(this);"><i
                                                    class="fa fa-minus"></i>
                                        </button>
                                                 <button class="btn btn-default" type="button"
                                                         data-target="browseServer-image"
                                                         onclick="showMultiImage(this);"><i class="fa fa-eye"></i>
                                                 </button>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Link
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="link" class="form-control" name="link" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="position">Vị trí
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" id="position" class="form-control" name="position" value="">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-offset-3">
                        <button type="submit" class="btn btn-success">Cập nhật</button>
                        <a href="<?php echo e(route('admin.settings.slider.index')); ?>">
                            <button type="button" class="btn btn-danger">Huỷ</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- My Modal Image -->
    <div id="myModalImage" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <img src="" class="image_preview img-responsive center-block">
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>