<?php $__env->startSection('title_for_layout'); ?>
    <title>Cài đặt chung</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('vendor/ckfinder/ckfinder.js')); ?>"></script>
    <script>
        CKEDITOR.replace( 'contactinfo' );
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thiết lập chung</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="app-tag" class="form-horizontal form-label-left"
                              action="<?php echo e(route('admin.settings.general.update')); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Tên công ty</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="name" required="required" class="form-control" name="name" value="<?php echo e(optional($setting)->name); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Tiêu đề website</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="title" required="required" class="form-control" name="title" value="<?php echo e(optional($setting)->title); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slogan">Slogan</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="slogan" class="form-control" name="slogan" rows="3"><?php echo e(optional($setting)->slogan); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="contactinfo">Tư vấn hỗ trợ</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="contactinfo" class="form-control" name="contactinfo" rows="5"><?php echo e(optional($setting)->contactinfo); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slogan">Địa chỉ</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="address" class="form-control" name="address" rows="3"><?php echo e(optional($setting)->address); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Điện thoại</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="phone" required="required" class="form-control" name="phone" value="<?php echo e(optional($setting)->phone); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Hotline</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="hotline" required="required" class="form-control" name="hotline" value="<?php echo e(optional($setting)->hotline); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="title">Email</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="email" required="required" class="form-control" name="email" value="<?php echo e(optional($setting)->email); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slogan">Google map</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="google_map" class="form-control" name="google_map" rows="3"><?php echo e(optional($setting)->google_map); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slogan">Keyword (SEO)</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="meta_key" class="form-control" name="meta_key" rows="3"><?php echo e(optional($setting)->meta_key); ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slogan">Description (SEO)</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="meta_des" class="form-control" name="meta_des" rows="3"><?php echo e(optional($setting)->meta_des); ?></textarea>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-8 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>