<div class="span8">
    <div class="flexslider">
        <ul class="slides">
            <?php if(isset($banners) && $banners->count()): ?>
                <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><a href="<?php echo e($banner->link); ?>"><img src="<?php echo e($banner->image); ?>" alt="<?php echo e($banner->name); ?>" /></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <li><a href="#"><img src="<?php echo e(asset('img/tong_cong_ty.jpg')); ?>" /></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>