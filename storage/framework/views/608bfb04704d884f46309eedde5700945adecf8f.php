<?php $__env->startSection('title_for_layout'); ?>
    <title>Danh mục tin tức</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý danh mục</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route("admin.orders.index")); ?>">Danh mục</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin danh mục</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table__management tbl-post-category">
                                <thead>
                                <tr>
                                    <th>
                                        <input class="i-checks check-all" type="checkbox">
                                    </th>
                                    <th> #</th>
                                    <th> Tên
                                    </th>
                                    <th> Mô tả
                                    </th>
                                    <th> Trạng thái
                                    </th>
                                    <th> <?php echo app('translator')->getFromJson('backend/table.common.updated_at'); ?> </th>
                                    <th><span class="nobr"> <?php echo app('translator')->getFromJson('backend/table.common.action'); ?> </span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="even pointer" data-id="<?php echo e($cat->id); ?>" data-model="Catalogue">
                                        <td><input type="checkbox" class="i-checks input_record" name="records[]"></td>
                                        <td>
                                            <?php echo e(($categories->currentPage() - 1) * $categories->perPage() + $key + 1); ?>

                                        </td>
                                        <td><a href="<?php echo e(route('admin.catalogues.edit', $cat->id)); ?>"> <?php echo e($cat->name_vi); ?> </a></td>
                                        <td> <?php echo e(str_limit($cat->description_vi, 100)); ?> </td>
                                        <td class="tbl__btn-status">
                                            <input class="btn-tbl-status" type="checkbox" name="status"
                                                   <?php echo e($cat->status == AppUtil::STATUS_ACTIVE ? "checked" : ""); ?> data-toggle="toggle"
                                                   data-size="mini" data-on="Active" data-off="Blocked"
                                                   data-onstyle="primary" data-offstyle="danger"/>
                                        </td>
                                        <td> <?php echo e($cat->updated_at->diffForHumans()); ?> </td>
                                        <td>
                                            <a href="<?php echo e(route('admin.catalogues.edit', $cat->id)); ?>"
                                               class="btn btn-info btn-xs tbl__btn-edit" title="Chi tiết"><i
                                                        class="fa fa-edit"></i></a>
                                            <form action="<?php echo e(route('admin.catalogues.destroy', $cat->id)); ?>"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <a class="btn btn-danger btn-xs tbl__btn-delete"><i
                                                            class="fa fa-trash"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tbl-footer">
                            <div class="tbl-footer--left">
                                <form class="tbl__frm-delete--multi" action="<?php echo e(route('admin.catalogues.delete.multi')); ?>"
                                      style="display: inline;" method="POST">
                                    <?php echo e(csrf_field()); ?>

                                    <input type="hidden" name="data_ids" value="" class="data_ids">
                                    <button type="button" class="btn btn-danger btn-sm tbl__btn-delete--multi">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <a href="<?php echo e(route('admin.catalogues.create')); ?>">
                                    <button class="btn btn-primary btn-sm tbl__btn-add">
                                        <i class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('backend/table.common.add_new'); ?>
                                    </button>
                                </a>
                            </div>
                            <div class="pagination-container pull-right no-margin">
                                <?php echo e($categories->links('vendor.pagination.custom')); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>