<!-- Toastr Message -->
<?php if(Session::has("flash_notification")): ?>

    <?php $__currentLoopData = session('flash_notification', collect())->toArray(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="alert alert-<?php echo e($message['level']); ?>">
            
            <?php echo e($message['message']); ?>

        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php echo e(session()->forget('flash_notification')); ?>


<?php endif; ?>