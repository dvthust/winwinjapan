<div class="row header"><!-- Begin Header -->

    <!-- Logo
    ================================================== -->
    <div class="span4 logo">
        <a href="<?php echo e(route('index')); ?>"><img src="<?php echo e(asset("images/logo.png")); ?>" alt="" /></a>
    </div>

    <div class="span4 slogan">
        <h4 class="slogan"><?php echo app('translator')->getFromJson('frontend/common.slogan'); ?></h4>
    </div>

    <div class="span4 language-change">
        <form class="change-lang-frm" method="POST" action="<?php echo e(route("changelocale")); ?>">
            <?php echo e(csrf_field()); ?>

            <?php if(App::isLocale('vi')): ?>
                <input type="hidden" name="lang" value="ja">
                <a class="language-item ja-lang" href="#"><img src="<?php echo e(asset('img/flag_ja.png')); ?>"></a>
            <?php else: ?>
                <input type="hidden" name="lang" value="vi">
                <a class="language-item ja-lang" href="#"><img src="<?php echo e(asset('img/flag_vn.png')); ?>"></a>
            <?php endif; ?>
        </form>
    </div>

    <!-- Main Navigation
    ================================================== -->
    <div class="span12 navigation winwin__navigation">
        <div class="navbar hidden-phone">

            <ul class="nav">
                <li class="dropdown active">
                    <a href="<?php echo e(route('index')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.home'); ?></a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo app('translator')->getFromJson('frontend/common.header.introduce'); ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(route('introduce')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.introduce'); ?></a></li>
                        <li><a href="<?php echo e(route('introduce.company')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.introduce_company'); ?></a></li>
                        <li><a href="<?php echo e(route('activities')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.activities'); ?></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo app('translator')->getFromJson('frontend/common.header.engineer'); ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(url('quy-trinh-tuyen-dung-ky-su')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.engineer_job'); ?> </a></li>
                        <li><a href="<?php echo e(url('quy-trinh-tuyen-dung-ky-su-co-khi')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.engineer_job_mechanical'); ?> </a></li>
                        <li><a href="<?php echo e(url('quy-trinh-tuyen-dung-ky-su-it')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.engineer_job_it'); ?> </a></li>
                    </ul>
                </li>
                <li><a href="<?php echo e(url('ky-su-co-khi')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.engineer_mechanical'); ?></a></li>
                <li><a href="<?php echo e(url('thuc-tap-sinh')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.apprentice'); ?></a></li>
                <li><a href="<?php echo e(url('du-hoc-han-quoc')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.student_korean'); ?></a></li>
                <li><a href="<?php echo e(url('du-hoc-nhat-ban')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.student_japan'); ?></a></li>
                <li><a href="<?php echo e(url('goc-tu-van')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.consultant'); ?></a></li>
                <li><a href="<?php echo e(route('contact')); ?>"><?php echo app('translator')->getFromJson('frontend/common.header.contact'); ?></a></li>
            </ul>

        </div>

        <!-- Mobile Nav
        ================================================== -->
        <form action="#" id="mobile-nav" class="visible-phone">
            <div class="mobile-nav-select">
                <select onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                    <option value="">Navigate...</option>
                    <option value="index.htm">Home</option>
                    <option value="index.htm">- Full Page</option>
                    <option value="index-gallery.htm">- Gallery Only</option>
                    <option value="index-slider.htm">- Slider Only</option>
                    <option value="features.htm">Features</option>
                    <option value="page-full-width.htm">Pages</option>
                    <option value="page-full-width.htm">- Full Width</option>
                    <option value="page-right-sidebar.htm">- Right Sidebar</option>
                    <option value="page-left-sidebar.htm">- Left Sidebar</option>
                    <option value="page-double-sidebar.htm">- Double Sidebar</option>
                    <option value="gallery-4col.htm">Gallery</option>
                    <option value="gallery-3col.htm">- 3 Column</option>
                    <option value="gallery-4col.htm">- 4 Column</option>
                    <option value="gallery-6col.htm">- 6 Column</option>
                    <option value="gallery-4col-circle.htm">- Gallery 4 Col Round</option>
                    <option value="gallery-single.htm">- Gallery Single</option>
                    <option value="blog-style1.htm">Blog</option>
                    <option value="blog-style1.htm">- Blog Style 1</option>
                    <option value="blog-style2.htm">- Blog Style 2</option>
                    <option value="blog-style3.htm">- Blog Style 3</option>
                    <option value="blog-style4.htm">- Blog Style 4</option>
                    <option value="blog-single.htm">- Blog Single</option>
                    <option value="page-contact.htm">Contact</option>
                </select>
            </div>
        </form>

    </div>
</div><!-- End Header -->