<?php $__env->startSection('title_for_layout'); ?>
    <title>Danh mục bài viết</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Quản lý bài viết</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo e(route("admin.posts.index")); ?>">Bài viết</a>
                </li>
                <li class="active">
                    <strong>Danh sách</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin bài viết</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <form action="<?php echo e(route("admin.posts.index")); ?>" method="GET">
                                <div class="col-sm-5 m-b-xs">
                                    <select class="input-sm form-control input-s-sm inline" name="category_id">
                                        <option value="0">--Tất cả danh mục--</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>" <?php echo e(app('request')->input('category_id') == $category->id ? "selected" : ""); ?>><?php echo e($category->name_vi); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_TO_DAY ? "active" : ""); ?>">
                                            <input type="radio"
                                                   id="day"
                                                   name="time"
                                                   <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_TO_DAY ? "checked" : ""); ?>

                                                   value="<?php echo e(\App\Eloquent\Post::$FILTER_TIME_TYPE_TO_DAY); ?>"> Day </label>
                                        <label class="btn btn-sm btn-white <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_WEEK ? "active" : ""); ?>">
                                            <input type="radio"
                                                   id="week"
                                                   name="time"
                                                   <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_WEEK ? "checked" : ""); ?>

                                                   value="<?php echo e(\App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_WEEK); ?>"> Week </label>
                                        <label class="btn btn-sm btn-white <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_MONTH ? "active" : ""); ?>">
                                            <input type="radio"
                                                   id="month"
                                                   name="time"
                                                   <?php echo e(app('request')->input('time') == \App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_MONTH ? "checked" : ""); ?>

                                                   value="<?php echo e(\App\Eloquent\Post::$FILTER_TIME_TYPE_THIS_MONTH); ?>"> Month </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text"
                                               placeholder="Tên"
                                               class="input-sm form-control"
                                               value="<?php echo e(app('request')->input('name_vi')); ?>"
                                               name="title">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-sm btn-primary"> Search</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table__management tbl-post-category">
                                <thead>
                                <tr>
                                    <th>
                                        <input class="i-checks check-all" type="checkbox">
                                    </th>
                                    <th> #</th>
                                    <th> Tên
                                    </th>
                                    <th> Nổi bật</th>
                                    <th> Trạng thái</th>
                                    <th> <?php echo app('translator')->getFromJson('backend/table.common.updated_at'); ?> </th>
                                    <th><span class="nobr"> <?php echo app('translator')->getFromJson('backend/table.common.action'); ?> </span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="even pointer" data-id="<?php echo e($post->id); ?>" data-model="Post">
                                        <td><input type="checkbox" class="i-checks input_record" name="records[]"></td>
                                        <td>
                                            <?php echo e(($posts->currentPage() - 1) * $posts->perPage() + $key + 1); ?>

                                        </td>
                                        <td><a href="<?php echo e(route('admin.posts.edit', $post->id)); ?>"> <?php echo e($post->name_vi); ?> </a></td>
                                        <td class="tbl__btn-highlight">
                                            <input class="btn-tbl-highlight" type="checkbox" name="highlight"
                                                   <?php echo e($post->highlight == AppUtil::STATUS_ACTIVE ? "checked" : ""); ?> data-toggle="toggle"
                                                   data-size="mini" data-on="Có" data-off="Không"
                                                   data-onstyle="primary" data-offstyle="danger"/>
                                        </td>
                                        <td class="tbl__btn-status">
                                            <input class="btn-tbl-status" type="checkbox" name="status"
                                                   <?php echo e($post->status == AppUtil::STATUS_ACTIVE ? "checked" : ""); ?> data-toggle="toggle"
                                                   data-size="mini" data-on="Active" data-off="Blocked"
                                                   data-onstyle="primary" data-offstyle="danger"/>
                                        </td>
                                        <td> <?php echo e($post->updated_at->diffForHumans()); ?> </td>
                                        <td>
                                            <a href="<?php echo e(route('admin.posts.edit', $post->id)); ?>"
                                               class="btn btn-info btn-xs tbl__btn-edit" title="Chi tiết"><i
                                                        class="fa fa-edit"></i></a>
                                            <form action="<?php echo e(route('admin.posts.destroy', $post->id)); ?>"
                                                  method="post"
                                                  class="tbl__frm-delete" style="display: inline">
                                                <?php echo e(method_field('DELETE')); ?>

                                                <?php echo e(csrf_field()); ?>

                                                <a class="btn btn-danger btn-xs tbl__btn-delete"><i
                                                            class="fa fa-trash"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tbl-footer">
                            <div class="tbl-footer--left">
                                <form class="tbl__frm-delete--multi" action="<?php echo e(route('admin.posts.delete.multi')); ?>"
                                      style="display: inline;" method="POST">
                                    <?php echo e(csrf_field()); ?>

                                    <input type="hidden" name="data_ids" value="" class="data_ids">
                                    <button type="button" class="btn btn-danger btn-sm tbl__btn-delete--multi">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </form>
                                <a href="<?php echo e(route('admin.posts.create')); ?>">
                                    <button class="btn btn-primary btn-sm tbl__btn-add">
                                        <i class="fa fa-plus"></i> <?php echo app('translator')->getFromJson('backend/table.common.add_new'); ?>
                                    </button>
                                </a>
                            </div>
                            <div class="pagination-container pull-right no-margin">
                                <?php echo e($posts->links('vendor.pagination.custom')); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>