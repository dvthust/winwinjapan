<?php $__env->startSection('title_for_layout'); ?>
    <title>404 Page Not Found Error</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <h1>Sorry, Page Not Found 404</h1>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>