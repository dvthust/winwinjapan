<?php $__env->startSection('title_for_layout'); ?>
    <title><?php echo app('translator')->getFromJson('frontend/common.header.home'); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row headline"><!-- Begin Headline -->

        <!-- Slider Carousel
       ================================================== -->
        <?php echo $__env->make("frontend.elements.banner", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- Headline Text
        ================================================== -->
        <div class="span4">
            <h3>Welcome to Piccolo. <br />
                A Big Theme in a Small Package.</h3>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium vulputate magna sit amet blandit.</p>
            <p>Cras rutrum, massa non blandit convallis, est lacus gravida enim, eu fermentum ligula orci et tortor. In sit amet nisl ac leo pulvinar molestie. Morbi blandit ultricies ultrices.</p>
            <a href="#"><i class="icon-plus-sign"></i>Chi tiết</a>
        </div>
    </div><!-- End Headline -->

    <div class="row gallery-row"><!-- Begin Gallery Row -->
        <div class="span12">
            <h5 class="title-bg title--general">Kỹ sư cơ khí</h5>
            <!-- Gallery Thumbnails
            ================================================== -->
            <div class="row clearfix no-margin">
                <ul class="gallery-post-grid holder">
                    <!-- Gallery Item 1 -->
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/洗浄機2.png")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 58</a>58-Thiết kế CAD2D, thiết kế máy</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/1.png")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 56</a>56-Phay CNC, Code G</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/tien_CNC.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 53</a>53-Tiện CNC, code G</span>
                    </li>
                </ul>
                <div class="view--all"><button class="btn btn-mini btn-inverse hidden-phone" type="button">Xem tất cả</button></div>
            </div>
        </div>
    </div><!-- End Gallery R -->

    <div class="row gallery-row"><!-- Begin Gallery Row -->
        <div class="span12">
            <h5 class="title-bg title--general">Thực tập sinh</h5>
            <!-- Gallery Thumbnails
            ================================================== -->
            <div class="row clearfix no-margin">
                <ul class="gallery-post-grid holder">
                    <!-- Gallery Item 1 -->
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/thuc-tap-sinh/7_on.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 2</a>Thực tập sinh đơn làm nấm</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/thuc-tap-sinh/36349450.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 4</a>Thực tập sinh đơn làm sơn</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/thuc-tap-sinh/All.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 6</a>Thực tập sinh đơn làm hàn</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/thuc-tap-sinh/z665345304556_8a56916424076323a78990607d565608.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 8</a>Thực tập sinh đơn làm chè</span>
                    </li>
                </ul>
                <div class="view--all"><button class="btn btn-mini btn-inverse hidden-phone" type="button">Xem tất cả</button></div>
            </div>
        </div>
    </div><!-- End Gallery R -->

    <div class="row gallery-row"><!-- Begin Gallery Row -->
        <div class="span12">
            <h5 class="title-bg title--general">Các hoạt động ngoại khoá</h5>
            <!-- Gallery Thumbnails
            ================================================== -->
            <div class="row clearfix no-margin">
                <div class="span6">
                    <div id="blogCarousel" class="carousel slide ">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <!-- Blog Item 1 -->
                            <div class="active item">
                                <a href="#">
                                    <img src="<?php echo e(asset("img/du-hoc-han-quoc/IT.jpg")); ?>" alt="" class="align-left blog-thumb-preview">
                                </a>
                                <h4><a href="#">Buổi học ngoại khóa luyện viết chữ.</a></h4>
                                <p class="blog-summary">
                                    Chữ viết là nền văn hóa cũng là biểu tượng của sự tri thức là tiếng nói riêng của mỗi nước ... <a href="#">Chi tiết</a></p><p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <ul class="popular-posts more-activities">
                        <li>
                            <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                            <h6><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h6>
                            <em>Posted on 09/01/15</em>
                        </li>
                        <li>
                            <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                            <h6><a href="#">Nulla iaculis mattis lorem, quis gravida nunc iaculis</a></h6>
                            <em>Posted on 09/01/15</em>
                        </li>
                        <li>
                            <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                            <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum</a></h6>
                            <em>Posted on 09/01/15</em>
                        </li>
                        <li>
                            <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                            <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum</a></h6>
                            <em>Posted on 09/01/15</em>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- End Gallery R -->

    <div class="row gallery-row"><!-- Begin Gallery Row -->
        <div class="span12">
            <h5 class="title-bg title--general">Du học hàn quốc</h5>
            <!-- Gallery Thumbnails
            ================================================== -->
            <div class="row clearfix no-margin">
                <ul class="gallery-post-grid holder">
                    <!-- Gallery Item 1 -->
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-han-quoc/IMG_0413.JPG")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu A </span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-han-quoc/IT.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu B </span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-han-quoc/Tuyển-gấp-kỹ-sư-It-xuat-khau-lao-dong-tại-Nhật-Bản-lương-cơ-bản-50tr-tháng.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu C </span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-han-quoc/z819005249515_6257115abbbc56a55952e35a5c7ff82c.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu D </span>
                    </li>
                </ul>
                <div class="view--all"><button class="btn btn-mini btn-inverse hidden-phone" type="button">Xem tất cả</button></div>
            </div>
        </div>
    </div><!-- End Gallery R -->

    <div class="row gallery-row"><!-- Begin Gallery Row -->
        <div class="span12">
            <h5 class="title-bg title--general">Du học nhật bản</h5>
            <!-- Gallery Thumbnails
            ================================================== -->
            <div class="row clearfix no-margin">
                <ul class="gallery-post-grid holder">
                    <!-- Gallery Item 1 -->
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-nhat-ban/1443171679_tim-hieu-truong-nhat-ngu-quoc-te-makuhari.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu tokyo</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-nhat-ban/1447302568_subaru.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu tokyo</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-nhat-ban/lopHoc.png")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu tokyo</span>
                    </li>
                    <li  class="span3 gallery-item" data-id="id-1" data-type="illustration">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/du-hoc-nhat-ban/tongdaihotrolaodong-japandotnet.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details">Du Học khu tokyo</span>
                    </li>
                </ul>
                <div class="view--all"><button class="btn btn-mini btn-inverse hidden-phone" type="button">Xem tất cả</button></div>
            </div>
        </div>
    </div><!-- End Gallery R -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>