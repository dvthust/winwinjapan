<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?php echo e(asset('vendor/inspinia/img/profile_small.jpg')); ?>" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold"><a href="<?php echo e(route("admin.users.profile", auth()->user()->id)); ?>"><?php echo e(auth()->user()->name); ?></a></strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    winwin
                </div>
            </li>
            <li>
                <a href="<?php echo e(route("admin.home.index")); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-first-order"></i> <span class="nav-label">Tuyển dụng</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo e(route("admin.catalogues.index")); ?>">Danh mục</a></li>
                    <li><a href="<?php echo e(route("admin.orders.index")); ?>">Quản lý Order</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label"><?php echo app('translator')->getFromJson('backend/sidebar.menu_section.content_management.news'); ?></span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo e(route("admin.categories.index")); ?>"><?php echo app('translator')->getFromJson('backend/sidebar.menu_section.content_management.news_category'); ?></a></li>
                    <li><a href="<?php echo e(route("admin.posts.index")); ?>"><?php echo app('translator')->getFromJson('backend/sidebar.menu_section.content_management.posts'); ?></a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo e(route("admin.supports.index")); ?>"><i class="fa fa-yelp"></i> <span class="nav-label">Quản lý chi nhánh</span></a>
            </li>
            <li>
                <a href="<?php echo e(route('admin.media.index')); ?>">
                    <i class="fa fa-film"></i><span class="nav-label"><?php echo app('translator')->getFromJson('backend/sidebar.menu_section.content_management.media'); ?></span>
                </a>
            </li>

            <li>
                <a href="<?php echo e(route("admin.settings.slider.index")); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Quản lý Slide</span></a>
            </li>

            <li>
                <a href="<?php echo e(route("admin.settings.general.index")); ?>"><i class="fa fa-gears"></i> <span class="nav-label"><?php echo app('translator')->getFromJson('backend/sidebar.menu_section.configuration.general_setting'); ?></span></a>
            </li>
            <li>
                <a href="<?php echo e(route("admin.users.profile", auth()->user()->id)); ?>"><i class="fa fa-user"></i> <span class="nav-label">Tài khoản</span></a>
            </li>
        </ul>

    </div>
</nav>