<?php $__env->startSection('title_for_layout'); ?>
    <title><?php echo app('translator')->getFromJson('frontend/common.header.home'); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row headline"><!-- Begin Headline -->

        <!-- Slider Carousel
       ================================================== -->
    <?php echo $__env->make("frontend.elements.banner", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Headline Text
        ================================================== -->
        <div class="span4">
            <h3>Welcome to Piccolo. <br />
                A Big Theme in a Small Package.</h3>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pretium vulputate magna sit amet blandit.</p>
            <p>Cras rutrum, massa non blandit convallis, est lacus gravida enim, eu fermentum ligula orci et tortor. In sit amet nisl ac leo pulvinar molestie. Morbi blandit ultricies ultrices.</p>
            <a href="#"><i class="icon-plus-sign"></i>Chi tiết</a>
        </div>
    </div><!-- End Headline -->

    <div class="row"><!--Container row-->
        <!-- Page Sidebar
        ================================================== -->
        <div class="span3 sidebar page-sidebar"><!-- Begin sidebar column -->
            <!--Goc Tu Van-->
            <h5 class="title-bg" style="margin-top: 0px;"><?php echo app('translator')->getFromJson('frontend/common.header.consultant'); ?></h5>
            <ul class="popular-posts">
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Nulla iaculis mattis lorem, quis gravida nunc iaculis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li><li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum maecenas felis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
            </ul>

            <!--Ky su co khi-->
            <h5 class="title-bg" style="margin-top: 0px;"><?php echo app('translator')->getFromJson('frontend/common.header.engineer_mechanical'); ?></h5>
            <ul class="popular-posts">
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
                <li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Nulla iaculis mattis lorem, quis gravida nunc iaculis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li><li>
                    <a href="#"><img src="img/gallery/gallery-img-2-thumb.jpg" alt="Popular Post"></a>
                    <h6><a href="#">Vivamus tincidunt sem eu magna varius elementum maecenas felis</a></h6>
                    <em>Posted on 09/01/15</em>
                </li>
            </ul>

        </div><!-- End sidebar column -->

        <!-- Page Content
        ================================================== -->
        <div class="span9 blog">
            <div class="row clearfix">
                <ul class="blog-post-grid">
                    <!-- Blog Post 1 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 2 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 3 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 4 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 5 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 6 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>
                    <!-- Blog Post 7 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>

                    <!-- Blog Post 8 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>

                    <!-- Blog Post 9 -->
                    <li class="span3 blog-post-item">
                        <a href="#"><img class="img-list-product" src="<?php echo e(asset("img/ks-co-khi/p03.jpg")); ?>" alt="Gallery"></a>
                        <span class="project-details"><a href="#">Mã Order: 57</a>57-PhayNC, GCTT, Cad2D</span>
                    </li>

                </ul>
            </div>

            <!-- Pagination -->
            <div class="pagination">
                <ul>
                    <li class="active"><a href="#">Prev</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>