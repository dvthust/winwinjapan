<?php $__env->startSection('scripts'); ?>
    <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('vendor/ckfinder/ckfinder.js')); ?>"></script>
    <script src="<?php echo e(asset("js/ck.js")); ?>"></script>

    <script>
        CKEDITOR.replace( 'content_vi' );
        CKEDITOR.replace( 'content_ja' );
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('title_for_layout'); ?>
    <title>Quản lý bài viết</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Quản lý bài viết</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Bài viết</a>
                </li>
                <li class="active">
                    <strong>Thêm mới</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Thông tin bài viết</h5>
                        <div class="pull-right">
                            <div id="language-group" class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle btn--language" data-toggle="dropdown" aria-expanded="false">
                                    <span class="language--span">Vietnamese</span>
                                    <i class="caret"></i>
                                </button>
                                <ul class="dropdown-menu pull-right language-list" role="menu">
                                    <li data-lang="vi" class="lang--option active"><a href="#">Vietnamese</a></li>
                                    <li data-lang="ja" class="lang--option"><a href="#">Japanese</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="app-category" class="form-label-left"
                              action="<?php echo e(route('admin.posts.store')); ?>" method="POST">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-group">
                                <label class="control-label" for="name">Tên bài viết
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <input type="text" class="form-control input-language" name="name_vi" data-lang="vi" value="">
                                    <input type="text" class="form-control input-language hidden" name="name_ja" data-lang="ja" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="name">Danh mục
                                    <span class="required">*</span>
                                </label>
                                <div class="">
                                    <select class="form-control" name="category_id">
                                        <option value="<?php echo e(0); ?>">--Lựa chọn--</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name_vi); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="short_des">Mô tả ngắn
                                </label>
                                <div class="input-language" data-lang="vi">
                                        <textarea class="form-control" name="short_des_vi" rows="5"></textarea>
                                </div>
                                <div class="input-language hidden" data-lang="ja">
                                        <textarea class="form-control" name="short_des_ja" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="content"> Nội dung
                                </label>
                                <div class="input-language" data-lang="vi">
                                        <textarea class="form-control" name="content_vi" rows="5"></textarea>
                                </div>
                                <div class="input-language hidden" data-lang="ja">
                                    <textarea class="form-control" name="content_ja" data-lang="ja" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputImage" class="control-label">
                                    Ảnh minh hoạ</label>
                                <div class="">
                                    <div class="form-group input-group">
                                        <input type="text" id="browseServer-image" name="feature_image" class="form-control"
                                               value="" placeholder="Nhấp chọn ảnh"
                                               onclick="selectFileWithCKFinder('browseServer-image');">
                                        <span class="input-group-btn">
                                        <button class="btn btn-danger" type="button" onclick="delSingleImg(this);"><i
                                                    class="fa fa-minus"></i>
                                        </button>
                                             <button class="btn btn-default" type="button"
                                                     data-target="browseServer-image"
                                                     onclick="showMultiImage(this);"><i class="fa fa-eye"></i>
                                             </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="short_des">Lượt view
                                </label>
                                <div class="">
                                    <input type="number" class="form-control" value="" name="view">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_title">Title Seo</label>
                                <div class="">
                                    <textarea id="meta_title" class="form-control" name="meta_title" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_keyword">Meta keyword</label>
                                <div class="">
                                    <textarea id="meta_keyword" class="form-control" name="meta_keyword" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="meta_description">Meta description</label>
                                <div class="">
                                    <textarea id="meta_description" class="form-control" name="meta_description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group" style="padding: 15px 0 25px 0;"><label class="col-xs-1 control-label" style="padding: 0;">Nổi bật</label>
                                <div class="col-sm-11">
                                    <label class="checkbox-inline">
                                        <input type="hidden" name="highlight" value="0" class="highlight--hidden">
                                        <input class="i-checks ckHighlight" type="checkbox" value="1" id="ckHighlight">
                                    </label>
                                    <label class="checkbox-inline"></label>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                    <a href="<?php echo e(route('admin.posts.index')); ?>"><button type="button" class="btn btn-danger">Huỷ</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- My Modal Image -->
    <div id="myModalImage" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <img src="" class="image_preview img-responsive center-block">
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>