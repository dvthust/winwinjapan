<?php $__env->startSection('title_for_layout'); ?>
    <title>Media Management</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="media-management" style="padding: 5px 0 40px 0;">
        <iframe id="ckfinder" src="<?php echo e(asset('vendor/ckfinder/ckfinder.html?gallery=0&langCode=vi')); ?>" frameborder="0" style="width: 100%; min-height: 600px"></iframe>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>